<?php
/*
    CanSeeEmulator
    Emulates a CanSee dongle hooked to a Renault ZOE

    Copyright (C) 2020 - The CanZE Team
    http://canze.fisch.lu

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or any
    later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
    
    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

$PAR = $_GET['command'];
$RES = "-";

$COMMAND = substr($PAR, 0, 1);
$ELEMENTS = explode(",", substr($PAR, 1));
$ID = $ELEMENTS[0];
switch ($COMMAND) {

    case "i":
        if (substr($ELEMENTS[1], 0, 1) == "1") {
            $SID = $ID . "." . "5" . substr($ELEMENTS[1], 1);
        } elseif (substr($ELEMENTS[1], 0, 1) == "2") {
            $SID = $ID . "." . "6" . substr($ELEMENTS[1], 1);
        } elseif (substr($ELEMENTS[1], 0, 1) == "3") {
            $SID = $ID . "." . "7" . substr($ELEMENTS[1], 1);
        }
        switch ($SID) {

            case "7bb.6101":
                $RES = "61011381138700000000000000000000000009980D61033A20D000000548000000000000000E721D000001142927100000000000";
                break;
            case "7bb.6161":
                $RES = "6161000972C0C8C8C8ACA40000DB8D00002D6DFF";
                break;
            case "7bb.6103":
                $RES = "610301972375242D0000000001980196000000FFFF07D00523D8000003";
                break;
            case "7bb.6104":
                $RES = "61040A84310A85310A86310A86310A83310A86310A7D310A7A310A86310A8B300A85310A8231FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF303031";
                break;
            case "7bb.6166":
                $RES = "61660001091B03AD056F00020001";
                break;
            case "7bb.6167":
                $RES = "616700000000000000000005001006094F806B414CFA00651402877B7AFD62640009008C022B012A00C100000000000000000000000000000000000000010000000000630052014200000000009E004301140000000000010000000000000000000000000000";
                break;
            case "7bb.6107":
                $RES = "6107000000000000000000000000";
                break;
            case "7bb.6106":
                $RES = "6106FFFFFFFF5807959F0000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000FFFFFFFF010000000000000000000000000000FF";
                break;
            case "7bb.6162":
                $RES = "616226009050";
                break;
            case "7bb.6130":
                $RES = "613022995D02003131002291530100313100227548020031310022753A000031310042853C0001313100428542000031310042854200003131004285420000313100228B45000031310043E745001132310023B1EC0200313100239DD90100313100236FCE03003131002375C30100323100236DBF0100323200";
                break;
            case "7bb.6131":
                $RES = "6131236DB600003232002361B80100323200234DB201003232002345A9010032320043E9A4000932320023C1E8010030300023A9DF0100303000239BD80100302F0023A1CF0000302F002399D10100302F002391C700003030002391CD0100302F00230BC9080132300022BB94060132310043E95D0010323200";
                break;
            case "7bb.6132":
                $RES = "613243D7EB00002F2F0023BFEB01002F2F0023ABE101002F2F002397D701002F2F00238FD00100302F002357C90401302F00235BAD00003030002333B5030031300023099E03013130002265730902333100220F35070233320021FD1A040133330022150A000032310043730A00123232002373B90000323200";
                break;
            case "7bb.6133":
                $RES = "613343C5B400042F2F0023C3E200002F2F00239DD10000302F002399CB0000302F002397CC0000302F002397CB0000302F004397CB00003030002397CB00003030002393CB000030300043E9C9000630300043E5F200003030002379F206003130002311CA070132310043118800003232002311880000323200";
                break;
            case "7bb.6134":
                $RES = "6134232B95000032320043E995000B32320023C7E9010030300023C3E40000303000";
                break;
            case "7bb.6141":
                $RES = "61410FE90FEB0FE40FEE0FE90FEA0FE10FE80FE80FE90FE90FE60FE90FE80FE90FEA0FEB0FEA0FE90FEC0FE40FEB0FEB0FE90FEA0FEC0FEB0FEB0FEB0FEB0FEB0FEC0FE90FEA0FEA0FEB0FEA0FEC0FEB0FEB0FE80FEA0FE90FEA0FE90FEA0FE90FEA0FEB0FEC0FE90FEA0FE80FEE0FEA0FEC0FE90FEA0FEA0FEA0FEB0FEB";
                break;
            case "7bb.6142":
                $RES = "61420FE90FEA0FE60FE80FE60FE80FE80FE60FE90FE90FE90FEB0FEA0FEA0FEA0FEA0FEA0FEA0FEA0FEB0FEB0FEC0FEB0FEC0FEA0FEB0FEA0FEA0FEC0FEC0FEB0FEB0FEC0FEB98CA9893";
                break;
            case "7bb.6180":
                $RES = "6180353937325205414530393433325206500000000002010088";
                break;
            case "7bb.6143":
                $RES = "6143000700060002000600070004000600060006000700070000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF";
                break;
            case "7bb.6184":
                $RES = "6184524C3046324439303930353044364E3130323734";
                break;
            case "7bb.6190":
                $RES = "619044364E3130323734";
                break;
            case "7bb.61FF":
                $RES = "61FF00000000004B4F5245414C47454F4C0000000000005CF1E6";
                break;
            case "7bb.61FE":
                $RES = "61FE353937325205414530393433325206500000000002010088";
                break;
            case "7bb.61F1":
                $RES = "61F100000000004B4F5245414C47454F4C0000000000005CF1E6";
                break;
            case "7bb.61F0":
                $RES = "61F0353937325205414530393433325206500000000002010088";
                break;
            case "7bb.61A0":
                $RES = "61A032393531303635303152544630303930353046464646464646";
                break;
            case "7bb.6160":
                $RES = "61601A8BFE6256";
                break;

            case "7ec.622001":
                $RES = "62200130";
                break;
            case "7ec.622002":
                $RES = "622002130A";
                break;
            case "7ec.622003":
                $RES = "6220030000";
                break;
            case "7ec.622005":
                $RES = "622005053D";
                break;
            case "7ec.622006":
                $RES = "62200600DB8C";
                break;
            case "7ec.62200B":
                $RES = "62200B02E1";
                break;
            case "7ec.62200C":
                $RES = "62200C0164";
                break;
            case "7ec.62200E":
                $RES = "62200E01";
                break;
            case "7ec.62200F":
                $RES = "62200F01";
                break;
            case "7ec.622021":
                $RES = "6220211388";
                break;
            case "7ec.622022":
                $RES = "6220221388";
                break;
            case "7ec.622025":
                $RES = "62202501";
                break;
            case "7ec.622026":
                $RES = "62202601";
                break;
            case "7ec.62202E":
                $RES = "62202E0000";
                break;
            case "7ec.622035":
                $RES = "622035FF";
                break;
            case "7ec.622036":
                $RES = "62203600";
                break;
            case "7ec.622039":
                $RES = "6220390F9A";
                break;
            case "7ec.62203A":
                $RES = "62203A01";
                break;
            case "7ec.62203B":
                $RES = "62203B01";
                break;
            case "7ec.62203C":
                $RES = "62203C01";
                break;
            case "7ec.62203D":
                $RES = "62203D01";
                break;
            case "7ec.62203E":
                $RES = "62203E01";
                break;
            case "7ec.62203F":
                $RES = "62203F00";
                break;
            case "7ec.622041":
                $RES = "62204101";
                break;
            case "7ec.622047":
                $RES = "62204701";
                break;
            case "7ec.62204B":
                $RES = "62204B00";
                break;
            case "7ec.62204C":
                $RES = "62204C05";
                break;
            case "7ec.622050":
                $RES = "6220500000";
                break;
            case "7ec.622051":
                $RES = "62205100";
                break;
            case "7ec.62205B":
                $RES = "62205B00";
                break;
            case "7ec.62206A":
                $RES = "62206A0000";
                break;
            case "7ec.62206B":
                $RES = "62206B0000";
                break;
            case "7ec.62206E":
                $RES = "62206E01";
                break;
            case "7ec.62207A":
                $RES = "62207A01";
                break;
            case "7ec.62207B":
                $RES = "62207B01";
                break;
            case "7ec.62207C":
                $RES = "62207C01";
                break;
            case "7ec.62207D":
                $RES = "62207D06";
                break;
            case "7ec.62207E":
                $RES = "62207E02";
                break;
            case "7ec.622166":
                $RES = "62216600";
                break;
            case "7ec.622181":
                $RES = "62218100000000";
                break;
            case "7ec.622184":
                $RES = "622184FFFF";
                break;
            case "7ec.622C04":
                $RES = "622C0401";
                break;
            case "7ec.622C2C":
                $RES = "622C2C00";
                break;
            case "7ec.622221":
                $RES = "62222100";
                break;
            case "7ec.622223":
                $RES = "62222300";
                break;
            case "7ec.622224":
                $RES = "62222400";
                break;
            case "7ec.622225":
                $RES = "62222500";
                break;
            case "7ec.622226":
                $RES = "62222600";
                break;
            case "7ec.622227":
                $RES = "62222700";
                break;
            case "7ec.622228":
                $RES = "62222800";
                break;
            case "7ec.62222B":
                $RES = "62222B01";
                break;
            case "7ec.62222F":
                $RES = "62222F00";
                break;
            case "7ec.622230":
                $RES = "62223000";
                break;
            case "7ec.622231":
                $RES = "62223100";
                break;
            case "7ec.622232":
                $RES = "62223200";
                break;
            case "7ec.622233":
                $RES = "62223300";
                break;
            case "7ec.622234":
                $RES = "62223400";
                break;
            case "7ec.622235":
                $RES = "62223500";
                break;
            case "7ec.622236":
                $RES = "62223601";
                break;
            case "7ec.622237":
                $RES = "62223700";
                break;
            case "7ec.622238":
                $RES = "62223801";
                break;
            case "7ec.622239":
                $RES = "6222390000";
                break;
            case "7ec.62223A":
                $RES = "62223A8000";
                break;
            case "7ec.62223B":
                $RES = "62223B8000";
                break;
            case "7ec.62223C":
                $RES = "62223CFFE0";
                break;
            case "7ec.62223D":
                $RES = "62223D8000";
                break;
            case "7ec.62223E":
                $RES = "62223E8000";
                break;
            case "7ec.62223F":
                $RES = "62223F8000";
                break;
            case "7ec.622241":
                $RES = "6222410000";
                break;
            case "7ec.622242":
                $RES = "6222428000";
                break;
            case "7ec.622243":
                $RES = "6222438000";
                break;
            case "7ec.622244":
                $RES = "62224403";
                break;
            case "7ec.622245":
                $RES = "6222458000";
                break;
            case "7ec.622246":
                $RES = "62224697C0";
                break;
            case "7ec.622247":
                $RES = "6222478000";
                break;
            case "7ec.622248":
                $RES = "6222488000";
                break;
            case "7ec.62224A":
                $RES = "62224A53CD";
                break;
            case "7ec.62224B":
                $RES = "62224B01";
                break;
            case "7ec.62224D":
                $RES = "62224D01";
                break;
            case "7ec.62224E":
                $RES = "62224E01";
                break;
            case "7ec.623001":
                $RES = "62300101";
                break;
            case "7ec.623006":
                $RES = "62300601";
                break;
            case "7ec.623007":
                $RES = "62300700";
                break;
            case "7ec.623008":
                $RES = "6230080310";
                break;
            case "7ec.623022":
                $RES = "62302201";
                break;
            case "7ec.623023":
                $RES = "6230231F";
                break;
            case "7ec.623024":
                $RES = "6230245F";
                break;
            case "7ec.623025":
                $RES = "62302507";
                break;
            case "7ec.623028":
                $RES = "62302808";
                break;
            case "7ec.623029":
                $RES = "62302916";
                break;
            case "7ec.62302A":
                $RES = "62302A02";
                break;
            case "7ec.623042":
                $RES = "6230428000";
                break;
            case "7ec.623043":
                $RES = "6230438000";
                break;
            case "7ec.623045":
                $RES = "6230458000";
                break;
            case "7ec.623047":
                $RES = "62304701B2";
                break;
            case "7ec.623101":
                $RES = "62310104";
                break;
            case "7ec.623103":
                $RES = "6231030000";
                break;
            case "7ec.623104":
                $RES = "62310400";
                break;
            case "7ec.623107":
                $RES = "62310700";
                break;
            case "7ec.623108":
                $RES = "62310801";
                break;
            case "7ec.623109":
                $RES = "6231090E";
                break;
            case "7ec.62310A":
                $RES = "62310A1C";
                break;
            case "7ec.62310B":
                $RES = "62310B02";
                break;
            case "7ec.62310D":
                $RES = "62310D02";
                break;
            case "7ec.62310E":
                $RES = "62310E02";
                break;
            case "7ec.623110":
                $RES = "623110FF";
                break;
            case "7ec.623111":
                $RES = "62311100";
                break;
            case "7ec.623112":
                $RES = "62311200";
                break;
            case "7ec.623113":
                $RES = "62311300";
                break;
            case "7ec.623201":
                $RES = "62320103";
                break;
            case "7ec.623202":
                $RES = "62320205";
                break;
            case "7ec.623203":
                $RES = "6232030310"; // 392V
                break;
            case "7ec.623204":
                $RES = "6232047F80"; // 32A
                break;
            case "7ec.623206":
                $RES = "62320656";
                break;
            case "7ec.623208":
                $RES = "62320800";
                break;
            case "7ec.623209":
                $RES = "62320901";
                break;
            case "7ec.62320A":
                $RES = "62320AA8";
                break;
            case "7ec.62320B":
                $RES = "62320B0D";
                break;
            case "7ec.62320C":
                $RES = "62320C0E7E";
                break;
            case "7ec.62320D":
                $RES = "62320D0000";
                break;
            case "7ec.623301":
                $RES = "623301DA";
                break;
            case "7ec.623302":
                $RES = "62330200";
                break;
            case "7ec.623307":
                $RES = "62330700";
                break;
            case "7ec.623308":
                $RES = "62330802";
                break;
            case "7ec.62330B":
                $RES = "62330B00";
                break;
            case "7ec.62330C":
                $RES = "62330C00";
                break;
            case "7ec.62330D":
                $RES = "62330D00";
                break;
            case "7ec.623310":
                $RES = "62331000";
                break;
            case "7ec.623311":
                $RES = "62331100";
                break;
            case "7ec.623312":
                $RES = "62331200";
                break;
            case "7ec.623313":
                $RES = "62331300";
                break;
            case "7ec.623315":
                $RES = "62331501";
                break;
            case "7ec.623316":
                $RES = "62331601";
                break;
            case "7ec.623317":
                $RES = "62331702";
                break;
            case "7ec.623318":
                $RES = "62331814";
                break;
            case "7ec.623319":
                $RES = "62331900";
                break;
            case "7ec.62331A":
                $RES = "62331A00";
                break;
            case "7ec.62331B":
                $RES = "62331B00";
                break;
            case "7ec.62331D":
                $RES = "62331D00";
                break;
            case "7ec.62331E":
                $RES = "62331E00";
                break;
            case "7ec.623322":
                $RES = "62332200";
                break;
            case "7ec.623325":
                $RES = "62332500";
                break;
            case "7ec.623326":
                $RES = "62332614";
                break;
            case "7ec.623327":
                $RES = "6233270A";
                break;
            case "7ec.623328":
                $RES = "6233280A";
                break;
            case "7ec.62332A":
                $RES = "62332A00";
                break;
            case "7ec.62332B":
                $RES = "62332B03";
                break;
            case "7ec.62332C":
                $RES = "62332C00";
                break;
            case "7ec.62332D":
                $RES = "62332D01";
                break;
            case "7ec.62332E":
                $RES = "62332E01";
                break;
            case "7ec.62332F":
                $RES = "62332F03";
                break;
            case "7ec.623330":
                $RES = "62333000";
                break;
            case "7ec.623331":
                $RES = "62333100";
                break;
            case "7ec.623332":
                $RES = "62333202";
                break;
            case "7ec.623335":
                $RES = "62333501";
                break;
            case "7ec.623336":
                $RES = "62333601";
                break;
            case "7ec.623337":
                $RES = "62333701";
                break;
            case "7ec.623338":
                $RES = "62333801";
                break;
            case "7ec.62333A":
                $RES = "62333A00";
                break;
            case "7ec.62333D":
                $RES = "62333D00";
                break;
            case "7ec.623348":
                $RES = "62334800";
                break;
            case "7ec.623349":
                $RES = "62334901172C09";
                break;
            case "7ec.62334A":
                $RES = "62334A0005D982";
                break;
            case "7ec.62334B":
                $RES = "62334B000BCA30";
                break;
            case "7ec.62334D":
                $RES = "62334D00000000";
                break;
            case "7ec.62334E":
                $RES = "62334E00000000";
                break;
            case "7ec.62334F":
                $RES = "62334F00000000";
                break;
            case "7ec.623351":
                $RES = "62335100000000";
                break;
            case "7ec.623352":
                $RES = "62335200000000";
                break;
            case "7ec.623353":
                $RES = "6233530132D698";
                break;
            case "7ec.62336A":
                $RES = "62336A43";
                break;
            case "7ec.62336B":
                $RES = "62336B42";
                break;
            case "7ec.62336C":
                $RES = "62336C3FFF";
                break;
            case "7ec.62336D":
                $RES = "62336D000000";
                break;
            case "7ec.62336E":
                $RES = "62336E0000";
                break;
            case "7ec.62336F":
                $RES = "62336F0000";
                break;
            case "7ec.623370":
                $RES = "6233708000";
                break;
            case "7ec.623371":
                $RES = "623371FE";
                break;
            case "7ec.623372":
                $RES = "62337200";
                break;
            case "7ec.623373":
                $RES = "62337303";
                break;
            case "7ec.623374":
                $RES = "62337400";
                break;
            case "7ec.623376":
                $RES = "6233760021";
                break;
            case "7ec.623379":
                $RES = "62337900";
                break;
            case "7ec.62337D":
                $RES = "62337D00";
                break;
            case "7ec.62337F":
                $RES = "62337F00";
                break;
            case "7ec.623381":
                $RES = "62338100";
                break;
            case "7ec.623384":
                $RES = "62338400";
                break;
            case "7ec.623386":
                $RES = "623386000000";
                break;
            case "7ec.623387":
                $RES = "62338701";
                break;
            case "7ec.623388":
                $RES = "62338800";
                break;
            case "7ec.623389":
                $RES = "62338900";
                break;
            case "7ec.62338A":
                $RES = "62338A01";
                break;
            case "7ec.62338B":
                $RES = "62338B01";
                break;
            case "7ec.62338C":
                $RES = "62338C00";
                break;
            case "7ec.62338D":
                $RES = "62338D00";
                break;
            case "7ec.62338E":
                $RES = "62338E00";
                break;
            case "7ec.62338F":
                $RES = "62338F00";
                break;
            case "7ec.623390":
                $RES = "62339000";
                break;
            case "7ec.623392":
                $RES = "62339200";
                break;
            case "7ec.623393":
                $RES = "62339300";
                break;
            case "7ec.623394":
                $RES = "62339401";
                break;
            case "7ec.623395":
                $RES = "62339500";
                break;
            case "7ec.623397":
                $RES = "62339700";
                break;
            case "7ec.623398":
                $RES = "62339800";
                break;
            case "7ec.623399":
                $RES = "62339900";
                break;
            case "7ec.62339A":
                $RES = "62339A01";
                break;
            case "7ec.62339B":
                $RES = "62339B00";
                break;
            case "7ec.62339C":
                $RES = "62339C01";
                break;
            case "7ec.62339D":
                $RES = "62339D00";
                break;
            case "7ec.62339E":
                $RES = "62339E00";
                break;
            case "7ec.62339F":
                $RES = "62339F00";
                break;
            case "7ec.6233A1":
                $RES = "6233A100";
                break;
            case "7ec.6233A2":
                $RES = "6233A200";
                break;
            case "7ec.6233A3":
                $RES = "6233A300";
                break;
            case "7ec.6233A4":
                $RES = "6233A400";
                break;
            case "7ec.6233A5":
                $RES = "6233A501";
                break;
            case "7ec.6233A8":
                $RES = "6233A805";
                break;
            case "7ec.6233A9":
                $RES = "6233A900";
                break;
            case "7ec.6233AA":
                $RES = "6233AA00";
                break;
            case "7ec.6233AB":
                $RES = "6233AB00";
                break;
            case "7ec.6233AC":
                $RES = "6233AC02";
                break;
            case "7ec.6233AD":
                $RES = "6233AD00";
                break;
            case "7ec.6233AF":
                $RES = "6233AF0AFC";
                break;
            case "7ec.6233B0":
                $RES = "6233B00D02";
                break;
            case "7ec.6233B1":
                $RES = "6233B130";
                break;
            case "7ec.6233B3":
                $RES = "6233B35A";
                break;
            case "7ec.6233B5":
                $RES = "6233B501";
                break;
            case "7ec.6233B6":
                $RES = "6233B600";
                break;
            case "7ec.6233B7":
                $RES = "6233B700";
                break;
            case "7ec.6233B8":
                $RES = "6233B803";
                break;
            case "7ec.6233B9":
                $RES = "6233B901";
                break;
            case "7ec.6233BB":
                $RES = "6233BB01";
                break;
            case "7ec.6233BD":
                $RES = "6233BD01";
                break;
            case "7ec.6233BE":
                $RES = "6233BE00";
                break;
            case "7ec.6233BF":
                $RES = "6233BF03";
                break;
            case "7ec.6233C1":
                $RES = "6233C101";
                break;
            case "7ec.6233C2":
                $RES = "6233C201";
                break;
            case "7ec.6233C3":
                $RES = "6233C304";
                break;
            case "7ec.6233C4":
                $RES = "6233C403";
                break;
            case "7ec.6233C5":
                $RES = "6233C501";
                break;
            case "7ec.6233C6":
                $RES = "6233C602";
                break;
            case "7ec.6233C7":
                $RES = "6233C704";
                break;
            case "7ec.6233C8":
                $RES = "6233C803";
                break;
            case "7ec.6233C9":
                $RES = "6233C900";
                break;
            case "7ec.6233CA":
                $RES = "6233CA00";
                break;
            case "7ec.6233CC":
                $RES = "6233CC00";
                break;
            case "7ec.6233CD":
                $RES = "6233CD00";
                break;
            case "7ec.6233CE":
                $RES = "6233CE00";
                break;
            case "7ec.6233CF":
                $RES = "6233CF00";
                break;
            case "7ec.6233D0":
                $RES = "6233D000";
                break;
            case "7ec.6233D1":
                $RES = "6233D100";
                break;
            case "7ec.6233D3":
                $RES = "6233D300000000000000000000";
                break;
            case "7ec.6233D4":
                $RES = "6233D400DACA00DAE200DB0800DB0800DB1D00DB3E00DB7500DB7500DB7600DB8C";
                break;
            case "7ec.6233D5":
                $RES = "6233D500000100000003030000";
                break;
            case "7ec.6233D6":
                $RES = "6233D600000000000000000000";
                break;
            case "7ec.6233D7":
                $RES = "6233D701F401E9014001F301F401F401B801E101F401F4";
                break;
            case "7ec.6233D8":
                $RES = "6233D8313031313232322F3032";
                break;
            case "7ec.6233D9":
                $RES = "6233D9007C0084000600A5007700B800A1002A00550085";
                break;
            case "7ec.6233de":

                if (!isset($_SESSION['7ec.6233de'])) $_SESSION['7ec.6233de'] = file_get_contents('7ec.6233de.txt');
                $_SESSION['7ec.6233de'] += rand(0, 50);
                file_put_contents('7ec.6233de.txt', $_SESSION['7ec.6233de']);
                $hex = dechex($_SESSION['7ec.6233de']);
                while (strlen($hex) < 8) $hex = '0' . $hex;
                $RES = "6233de" . $hex;
                break;
            case "7ec.6233E1":
                $RES = "6233E110BFFD00";
                break;
            case "7ec.6233E2":
                $RES = "6233E204";
                break;
            case "7ec.6233E3":
                $RES = "6233E306";
                break;
            case "7ec.6233E4":
                $RES = "6233E400";
                break;
            case "7ec.6233E6":
                $RES = "6233E605";
                break;
            case "7ec.6233E7":
                $RES = "6233E70000";
                break;
            case "7ec.6233E8":
                $RES = "6233E802";
                break;
            case "7ec.6233E9":
                $RES = "6233E902";
                break;
            case "7ec.6233EA":
                $RES = "6233EA00";
                break;
            case "7ec.6233EC":
                $RES = "6233EC01";
                break;
            case "7ec.6233ED":
                $RES = "6233ED00";
                break;
            case "7ec.6233EF":
                $RES = "6233EF00";
                break;
            case "7ec.6233F0":
                $RES = "6233F000";
                break;
            case "7ec.6233F1":
                $RES = "6233F1022E";
                break;
            case "7ec.6233F2":
                $RES = "6233F230";
                break;
            case "7ec.6233F3":
                $RES = "6233F33106";
                break;
            case "7ec.6233F4":
                $RES = "6233F41F69";
                break;
            case "7ec.6233F5":
                $RES = "6233F532";
                break;
            case "7ec.6233F6":
                $RES = "6233F632";
                break;
            case "7ec.6233F7":
                $RES = "6233F700002C";
                break;
            case "7ec.6233F8":
                $RES = "6233F800";
                break;
            case "7ec.6233F9":
                $RES = "6233F901";
                break;
            case "7ec.6233FA":
                $RES = "6233FA00";
                break;
            case "7ec.6233FB":
                $RES = "6233FB00";
                break;
            case "7ec.6233FC":
                $RES = "6233FC02";
                break;
            case "7ec.6233FD":
                $RES = "6233FD00";
                break;
            case "7ec.6233FE":
                $RES = "6233FE00";
                break;
            case "7ec.623401":
                $RES = "62340101";
                break;
            case "7ec.623402":
                $RES = "62340201";
                break;
            case "7ec.623403":
                $RES = "62340301";
                break;
            case "7ec.623404":
                $RES = "62340401";
                break;
            case "7ec.623405":
                $RES = "62340501";
                break;
            case "7ec.623407":
                $RES = "62340701";
                break;
            case "7ec.623408":
                $RES = "62340801";
                break;
            case "7ec.623409":
                $RES = "62340901";
                break;
            case "7ec.62340A":
                $RES = "62340A01";
                break;
            case "7ec.62340B":
                $RES = "62340B01";
                break;
            case "7ec.62340C":
                $RES = "62340C01";
                break;
            case "7ec.62340D":
                $RES = "62340D01";
                break;
            case "7ec.62340E":
                $RES = "62340E01";
                break;
            case "7ec.62340F":
                $RES = "62340F01";
                break;
            case "7ec.623418":
                $RES = "62341800";
                break;
            case "7ec.623419":
                $RES = "62341900";
                break;
            case "7ec.62341A":
                $RES = "62341A00";
                break;
            case "7ec.62341E":
                $RES = "62341E0000";
                break;
            case "7ec.623423":
                $RES = "62342301";
                break;
            case "7ec.623424":
                $RES = "62342401";
                break;
            case "7ec.623425":
                $RES = "62342500";
                break;
            case "7ec.623426":
                $RES = "62342600";
                break;
            case "7ec.6180":
                $RES = "6180303039315205303031303035345202030680038501010188";
                break;
            case "7ec.61F0":
                $RES = "61F0303037365205303031303035345202030680000001010088";
                break;
            case "7ec.61F1":
                $RES = "61F1303030303046464F495820202020200013071518545C9DE4";
                break;
            case "7ec.623383":
                $RES = "6233830000000000";
                break;
            case "7ec.623396":
                $RES = "62339600";
                break;
            case "7ec.62337E":
                $RES = "62337E0000";
                break;
            case "7ec.623391":
                $RES = "62339101";
                break;
            case "7ec.623378":
                $RES = "62337802";
                break;
            case "7ec.6233EE":
                $RES = "6233EE2710";
                break;
            case "7ec.6233CB":
                $RES = "6233CB00";
                break;
            case "7ec.623364":
                $RES = "62336400000000";
                break;
            case "7ec.62335F":
                $RES = "62335F00000000";
                break;
            case "7ec.623361":
                $RES = "623361005ED551";
                break;
            case "7ec.623362":
                $RES = "6233620002140A";
                break;
            case "7ec.623363":
                $RES = "623363000019E3";
                break;
            case "7ec.623365":
                $RES = "62336500000000";
                break;
            case "7ec.623369":
                $RES = "62336900000000";
                break;
            case "7ec.623366":
                $RES = "623366000BA9C4";
                break;
            case "7ec.623367":
                $RES = "62336700389B06";
                break;
            case "7ec.623368":
                $RES = "62336800831DF1";
                break;
            case "7ec.623359":
                $RES = "62335900000007";
                break;
            case "7ec.623355":
                $RES = "6233550000F718";
                break;
            case "7ec.623356":
                $RES = "62335600206D35";
                break;
            case "7ec.623357":
                $RES = "6233570033C471";
                break;
            case "7ec.623358":
                $RES = "623358000BDA76";
                break;
            case "7ec.62335E":
                $RES = "62335E00000000";
                break;
            case "7ec.62335A":
                $RES = "62335A0000BC69";
                break;
            case "7ec.62335B":
                $RES = "62335B0006DCFE";
                break;
            case "7ec.62335C":
                $RES = "62335C001FE1A1";
                break;
            case "7ec.62335D":
                $RES = "62335D009FE7B3";
                break;
            case "7ec.62224C":
                $RES = "62224C01";
                break;
            case "7ec.623410":
                $RES = "62341001";
                break;
            case "7ec.6233FF":
                $RES = "6233FF00";
                break;
            case "7ec.623416":
                $RES = "62341600E8018D018D01E20238023800AF000000060055";
                break;
            case "7ec.623413":
                $RES = "62341306A2094309430ADE0D500D50046D00000013019F";
                break;
            case "7ec.623412":
                $RES = "62341207910AC30AC30CAF0F910F91051E000000200206";
                break;
            case "7ec.623414":
                $RES = "6234140020002100210022004500450021000000020005";
                break;
            case "7ec.623415":
                $RES = "623415131A0C851380138813881329113412CD13881388";
                break;
            case "7ec.622249":
                $RES = "6222498000";
                break;
            case "7ec.6233BA":
                $RES = "6233BA00";
                break;
            case "7ec.623421":
                $RES = "62342128";
                break;
            case "7ec.623429":
                $RES = "62342901";
                break;
            case "7ec.62342A":
                $RES = "62342A0000";
                break;
            case "7ec.623411":
                $RES = "62341100DAE200DB0800DB0800DB1D00DB3E00DB3E00DB7500DB7500DB7600DB8C";
                break;
            case "7ec.623375":
                $RES = "6233750101010101";
                break;
            case "7ec.622079":
                $RES = "62207964";
                break;
            case "7ec.622078":
                $RES = "62207800";
                break;
            case "7ec.62204F":
                $RES = "62204F80";
                break;
            case "7ec.62204E":
                $RES = "62204E20";
                break;
            case "7ec.62204D":
                $RES = "62204D00";
                break;
            case "7ec.623431":
                $RES = "623431000001FA";
                break;
            case "7ec.623432":
                $RES = "6234326418";
                break;
            case "7ec.623433":
                $RES = "6234331E";
                break;
            case "7ec.62342D":
                $RES = "62342D00";
                break;
            case "7ec.6184":
                $RES = "61843030314631333037313536353831393633202020";
                break;
            case "7ec.6181":
                $RES = "618156463141475659413034393539373038358314";
                break;
            case "7ec.62342E":
                $RES = "62342E01";
                break;
            case "7ec.623434":
                $RES = "62343400";
                break;
            case "7ec.6233BC":
                $RES = "6233BC01";
                break;
            case "7ec.623428":
                $RES = "6234286A72";
                break;
            case "7ec.6233DF":
                $RES = "6233DF10BFFD00";
                break;
            case "7ec.6233DE":
                $RES = "ATFCSH77B command problem";
                break;
            case "7ec.6233DD":
                $RES = "6233DD029C75";
                break;
            case "7ec.6233DC":
                $RES = "6233DC88267C";
                break;
            case "7ec.6233DB":
                $RES = "6233DB585E";
                break;
            case "7ec.623461":
                $RES = "62346101";
                break;
            case "7ec.623462":
                $RES = "62346201";
                break;
            case "7ec.623464":
                $RES = "62346400";
                break;
            case "7ec.623465":
                $RES = "6234658000";
                break;
            case "7ec.623435":
                $RES = "62343501";
                break;
            case "7ec.623436":
                $RES = "6234360000";
                break;
            case "7ec.623437":
                $RES = "623437C3";
                break;
            case "7ec.623438":
                $RES = "6234383B";
                break;
            case "7ec.623439":
                $RES = "62343900";
                break;
            case "7ec.62343A":
                $RES = "62343A00";
                break;
            case "7ec.62343B":
                $RES = "62343B01";
                break;
            case "7ec.62343C":
                $RES = "62343C00";
                break;
            case "7ec.62343D":
                $RES = "62343D00000000000000000000000000000000000000000000000000000000000000000000000026009050";
                break;
            case "7ec.62343F":
                $RES = "62343F26009050";
                break;
            case "7ec.623441":
                $RES = "62344100";
                break;
            case "7ec.623442":
                $RES = "62344201";
                break;
            case "7ec.623443":
                $RES = "62344300";
                break;
            case "7ec.623444":
                $RES = "62344406";
                break;
            case "7ec.623445":
                $RES = "6234450000";
                break;
            case "7ec.623446":
                $RES = "62344600";
                break;
            case "7ec.623448":
                $RES = "62344897C0";
                break;
            case "7ec.62344A":
                $RES = "62344A00";
                break;
            case "7ec.62344B":
                $RES = "62344B01";
                break;
            case "7ec.62344C":
                $RES = "62344C00";
                break;
            case "7ec.62344D":
                $RES = "62344D00";
                break;
            case "7ec.62344E":
                $RES = "62344E00";
                break;
            case "7ec.62344F":
                $RES = "62344F00";
                break;
            case "7ec.623450":
                $RES = "62345000";
                break;
            case "7ec.623451":
                $RES = "6234510062";
                break;
            case "7ec.623453":
                $RES = "62345300";
                break;
            case "7ec.623452":
                $RES = "62345200";
                break;
            case "7ec.623454":
                $RES = "623454002D770010F2";
                break;
            case "7ec.623455":
                $RES = "62345523F1";
                break;
            case "7ec.623456":
                $RES = "6234560067";
                break;
            case "7ec.623457":
                $RES = "62345735C0";
                break;
            case "7ec.623458":
                $RES = "6234580044";
                break;
            case "7ec.623459":
                $RES = "62345925C0";
                break;
            case "7ec.62345A":
                $RES = "62345A4E";
                break;
            case "7ec.62345B":
                $RES = "62345B60";
                break;
            case "7ec.62345C":
                $RES = "62345C50";
                break;
            case "7ec.62345D":
                $RES = "62345D00";
                break;
            case "7ec.62345E":
                $RES = "62345E05";
                break;
            case "7ec.62345F":
                $RES = "62345F03DD";
                break;
            case "7ec.623463":
                $RES = "62346300";
                break;
            case "7ec.622093":
                $RES = "62209303";
                break;
            case "7ec.62212E":
                $RES = "62212E00";
                break;
            case "7ec.622170":
                $RES = "62217000";
                break;
            case "7ec.6233EB":
                $RES = "6233EB03";
                break;
            case "7ec.623422":
                $RES = "62342228";
                break;
            case "7ec.623466":
                $RES = "62346600";
                break;
            case "7ec.623469":
                $RES = "62346900";
                break;
            case "7ec.62346A":
                $RES = "62346A32";
                break;
            case "7ec.62346B":
                $RES = "62346B01";
                break;
            case "7ec.62346C":
                $RES = "62346C01";
                break;
            case "7ec.62346D":
                $RES = "62346D01";
                break;
            case "7ec.62346E":
                $RES = "62346E00";
                break;
            case "7ec.62346F":
                $RES = "62346F00";
                break;
            case "7ec.623470":
                $RES = "ATFCSD300010 command problem";
                break;
            case "7ec.623471":
                $RES = "62347100";
                break;
            case "7ec.623472":
                $RES = "6234720000";
                break;
            case "7ec.623473":
                $RES = "6234730000";
                break;
            case "7ec.623474":
                $RES = "62347401";
                break;
            case "7ec.623475":
                $RES = "62347501";
                break;
            case "7ec.623476":
                $RES = "623476000000000000";
                break;
            case "7ec.623477":
                $RES = "62347723F2BE";
                break;
            case "7ec.623478":
                $RES = "62347825C147";
                break;
            case "7ec.623479":
                $RES = "623479014400";
                break;
            case "7ec.62347A":
                $RES = "62347A00";
                break;
            case "7ec.62347B":
                $RES = "62347B00";
                break;
            case "7ec.62347C":
                $RES = "62347C00";
                break;
            case "7ec.62347D":
                $RES = "62347D00";
                break;
            case "7ec.62347E":
                $RES = "62347E05B400";
                break;
            case "7ec.62347F":
                $RES = "62347F00";
                break;
            case "7ec.623481":
                $RES = "62348100";
                break;
            case "7ec.623482":
                $RES = "ATFCSM1 command problem";
                break;
            case "7ec.623483":
                $RES = "623483001A965B";
                break;
            case "7ec.623484":
                $RES = "6234847F7F";
                break;
            case "7ec.623485":
                $RES = "6234852A89";
                break;
            case "7ec.623486":
                $RES = "62348600";
                break;
            case "7ec.623487":
                $RES = "6234870003FBCC";
                break;
            case "7ec.623309":
                $RES = "62330901";
                break;
            case "7ec.62330A":
                $RES = "62330A01";
                break;
            case "7ec.6233A6":
                $RES = "6233A600";
                break;
            case "7ec.623417":
                $RES = "6234170000";
                break;
            case "7ec.622004":
                $RES = "6220040311";
                break;
            case "7ec.623427":
                $RES = "62342700";
                break;
            case "7ec.61B7":
                $RES = "61B702F8000000000000000000";
                break;
            case "7ec.61B8":
                $RES = "61B802F8000000000000000000";
                break;
            case "7ec.623449":
                $RES = "62344900000000000000000000000000000000000000000000000000000000000000000000000000000000";
                break;
            case "7ec.623382":
                $RES = "62338200000000000000000002";
                break;
            case "7ec.6233AE":
                $RES = "6233AE0000";
                break;
            case "7ec.6233A7":
                $RES = "6233A700";
                break;
            case "7ec.623377":
                $RES = "62337702";
                break;
            case "7ec.622875":
                $RES = "6228750000";
                break;

            case "764.6182":
                $RES = "618210030000FF000000FFFFFF00FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00FFFFFFFFFFFFFFFF";
                break;
            case "764.62f18e":
                $RES = "62F18E32383532353930323552";
                break;
            case "764.6180":
                $RES = "6180383031395204303432393530315206020515230802010088";
                break;
            case "764.629102":
                $RES = "6291022308";
                break;
            case "764.629101":
                $RES = "6291010515";
                break;
            case "764.6127":
                $RES = "6127646464646400647F7F6464000000007F";
                break;
            case "764.6126":
                $RES = "612600000000000000000000000000000000";
                break;
            case "764.6125":
                $RES = "612500000000000000000000000000000000";
                break;
            case "764.6128":
                $RES = "612801100000002A010080A00000010000000000";
                break;
            case "764.6121":
                $RES = "612177E0570FFFFFFFF0047F008700813C2011DE00";
                break;
            case "764.6130":
                $RES = "613003E803E803E8007F007F0000";
                break;
            case "764.6103":
                $RES = "610310";
                break;
            case "764.610b":
                $RES = "610B01";
                break;
            case "764.6105":
                $RES = "610501";
                break;
            case "764.6111":
                $RES = "611101";
                break;
            case "764.6107":
                $RES = "610721";
                break;
            case "764.6114":
                $RES = "61142FFF1FFC40007E600000FF87FFCFFC00";
                break;
            case "764.6109":
                $RES = "610902";
                break;
            case "764.610e":
                $RES = "610E02";
                break;
            case "764.6104":
                $RES = "610418";
                break;
            case "764.6108":
                $RES = "610864";
                break;
            case "764.610a":
                $RES = "610A02";
                break;
            case "764.6106":
                $RES = "610602";
                break;
            case "764.6102":
                $RES = "610210";
                break;
            case "764.610c":
                $RES = "610C01";
                break;
            case "764.62F187":
                $RES = "62F18732383532353930323552";
                break;
            case "764.6181":
                $RES = "618156463141475659413034393539373038358314";
                break;
            case "764.6184":
                $RES = "61843637383131333031373430362020202020202020";
                break;
            case "764.6116":
                $RES = "611602";
                break;
            case "764.6117":
                $RES = "611702";
                break;
            case "764.611a":
                $RES = "611A02";
                break;
            case "764.6119":
                $RES = "611902";
                break;
            case "764.610d":
                $RES = "610D01";
                break;
            case "764.610f":
                $RES = "610F03";
                break;
            case "764.611b":
                $RES = "611B02";
                break;
            case "764.611e":
                $RES = "611E02";
                break;
            case "764.612b":
                $RES = "612B02";
                break;
            case "764.612c":
                $RES = "612C01";
                break;
            case "764.6146":
                $RES = "6146FECBFFFC07D00000000000";
                break;
            case "764.6171":
                $RES = "617102";
                break;
            case "764.6172":
                $RES = "617202";
                break;
            case "764.6173":
                $RES = "617302";
                break;
            case "764.6143":
                $RES = "6143FF000FF0BFC00000F9400000C0F10043FFFE0FFFF01F893C60";
                break;
            case "764.6144":
                $RES = "61441F40001E10038000FF901000012026C01F413E1E1784000000";
                break;
            case "764.6145":
                $RES = "614500114401F40209FC0020806000000000000000000000000000";
                break;
            case "764.6161":
                $RES = "616100F400FF00000000";
                break;
            case "764.6162":
                $RES = "61620007027D80FFC07B";
                break;
            case "764.6163":
                $RES = "6163E28510E1F9300000";
                break;
            case "764.6164":
                $RES = "6164700032990C40FFFF";
                break;
            case "764.6165":
                $RES = "61652A000A00008000FF";
                break;
            case "764.6166":
                $RES = "616632800000000200E0";
                break;
            case "764.6167":
                $RES = "616707000084F1840000";
                break;
            case "764.6168":
                $RES = "6168307000000003FFFC";
                break;
            case "764.6169":
                $RES = "61690191964FE0000000";
                break;
            case "764.6115":
                $RES = "611503";
                break;
            case "764.6174":
                $RES = "617401";
                break;
            case "764.6175":
                $RES = "617501";
                break;
            case "764.612D":
                $RES = "612D01";
                break;

            case "763.6180":
                $RES = "6180303834345224303230373831395205070504050302010088";
                break;

            case "77e.623024":
                $RES = "623024C000";
                break;

            case "765.6174": // status and pressures
                $RES = "617400000000000000000000000000000000000000";
                break;
            case "765.50c0": // tester
                $RES = "50c0";
                break;
            case "765.7e01": // tester
                $RES = "7e01";
                break;
            case "765.6171": // get ids
                $RES = "617180111111222222333333444444";
                break;
            case "765.7b5e01111111": // get ids
                $RES = "7b5e01111111";
                break;
            case "765.7b5e02222222": // get ids
                $RES = "7b5e02222222";
                break;
            case "765.7b5e03444444": // get ids
                $RES = "7b5e03444444";
                break;
            case "765.7b5e04333333": // get ids
                $RES = "7b5e04333333";
                break;

            case "18daf1d2.5003":
                $RES = "5003003201f4";
                break;
            case "18daf1da.5003":
                $RES = "5003003201f4";
                break;
            case "18daf1da.62202e":
                $RES = "62202E0300";
                break;

            default:
                $RES = "-E-Frame not found ($SID)";
                break;
        }
        break;

    case "g":

        switch ($ID) {
            case "17a":
                $RES = "FFFFFFAA00C031A3";
                break;
            case "186":
                $RES = "00003203200020";
                break;
            case "1f6":
                $RES = "DE04002B00FF00FF";
                break;
            case "0c6":
                $RES = "805F7FFF80128C84";
                break;
            case "12e":
                $RES = "C87FFF7FE0FFFF00";
                break;
            case "29a":
                $RES = "00000000000008F7";
                break;
            case "130":
                $RES = "0008AFFE009FFEAD";
                break;
            case "2b7":
                $RES = "00E0FFFE94";
                break;
            case "1f8":
                $RES = "FE04FFEFFE00040D";
                break;
            case "17e":
                $RES = "FFFFFF00FF4000FF";
                break;
            case "18a":
                $RES = "FFF000064000";
                break;
            case "650":
                $RES = "4115FA16C003FE";
                break;
            case "5df":
                $RES = "A8";
                break;
            case "427":
                $RES = "97FFFFC7FF070B50";
                break;
            case "242":
                $RES = "0200FFEFFE000C";
                break;
            case "29c":
                $RES = "00000000FFFF0000";
                break;
            case "354":
                $RES = "0000000000000000";
                break;
            case "391":
                $RES = "00000000C000";
                break;
            case "212":
                $RES = "FE1DC04C0000";
                break;
            case "42e":
                $RES = "213FD0D78406E085";
                break;
            case "66a":
                $RES = "00FE00000000";
                break;
            case "5ee":
                $RES = "0000011E7D800080";
                break;
            case "652":
                $RES = "DA4FFF1541235551";
                break;
            case "656":
                $RES = "07FF07FEFFC03600";
                break;
            case "3b7":
                $RES = "FFFF4CCFFE";
                break;
            case "66d":
                $RES = "AA80";
                break;
            case "3f7":
                $RES = "0800";
                break;
            case "793":
                $RES = "2EFD500649945003";
                break;
            case "792":
                $RES = "300000FFFFFFFFFF";
                break;
            case "511":
                $RES = "00E9A514D97254";
                break;
            case "500":
                $RES = "02A204A1BC";
                break;
            case "352":
                $RES = "5C000000";
                break;
            case "65b":
                $RES = "000000FC4104";
                break;
            case "657":
                $RES = "C4C8";
                break;
            case "653":
                $RES = "00415800";
                break;
            case "5d7": // speed = 16bit
                if (!isset($_SESSION['5d7.0'])) $_SESSION['5d7.0'] = file_get_contents('5d7.0.txt');
                if ($_SESSION['5d7.0'] < 3000) $_SESSION['5d7.0'] += rand(-10, 40);
                else if ($_SESSION['5d7.0'] < 6000) $_SESSION['5d7.0'] += rand(-20, 30);
                else if ($_SESSION['5d7.0'] < 9000) $_SESSION['5d7.0'] += rand(-30, 25);
                else                              $_SESSION['5d7.0'] += rand(-40, 10);
                if ($_SESSION['5d7.0'] < 0) $_SESSION['5d7.0'] = 0;
                else if ($_SESSION['5d7.0'] > 14000) $_SESSION['5d7.0'] = 14000;
                file_put_contents('5d7.0.txt', $_SESSION['5d7.0']);
                $hex = dechex($_SESSION['5d7.0']);
                while (strlen($hex) < 4) $hex = '0' . $hex;
                $RES = $hex . "05E777E0D4";
                error_log("Speed = " . $_SESSION['5d7.0'] . " --> " . $hex . " ---> " . $RES, 0);
                //$RES = "000905E777E0D4";
                $RES = "140e05E777E0D4";
                break;
            case "42a":
                $RES = "023E00024400E000";
                break;
            case "430":
                $RES = "000000176084E130";
                break;
            case "634":
                $RES = "800052";
                break;
            case "668":
                $RES = "8206";
                break;
            case "699":
                $RES = "00000200BF000004";
                break;
            case "432":
                $RES = "5000020B7C800058";
                break;
            case "68b":
                $RES = "F200";
                break;
            case "4f8":
                $RES = "8410000000";
                break;
            case "638":
                $RES = "5001C2500000";
                break;
            case "62c":
                $RES = "00";
                break;
            case "1fd": // 48-55 = power --> 8bit
                if (!isset($_SESSION['1fd.48'])) $_SESSION['1fd.48'] = file_get_contents('1fd.48.txt');
                if ($_SESSION['1fd.48'] < 80) $_SESSION['1fd.48'] += rand(-2, 4);
                else if ($_SESSION['1fd.48'] < 100) $_SESSION['1fd.48'] += rand(-3, 3);
                else if ($_SESSION['1fd.48'] < 160) $_SESSION['1fd.48'] += rand(-3, 2);
                if ($_SESSION['1fd.48'] < 0) $_SESSION['1fd.48'] = 0;
                else if ($_SESSION['1fd.48'] > 145) $_SESSION['1fd.48'] = 145;
                file_put_contents('1fd.48.txt', $_SESSION['1fd.48']);

                // $_SESSION['1fd.48']=16+80; // fix value
                $hex = dechex($_SESSION['1fd.48']);
                if (strlen($hex) < 2) $hex = '0' . $hex;

                //$RES="094008FF7FC89600"; // 70 kW
                $RES = "000000000000" . $hex . "00";
                //error_log("Power = ".$_SESSION['1fd.48']." --> ".$hex." ---> ".$RES, 0);	
                break;
            case "5ef":
                $RES = "008000";
                break;
            case "35c":
                $RES = "F2C61443BF3F6424";
                break;
            case "666":
                $RES = "30000000";
                break;
            case "552":
                $RES = "0F54";
                break;
            case "563":
                $RES = "07C0";
                break;
            case "5de":
                $RES = "000A0000E1000040";
                break;
            case "5e9":
                $RES = "0038000000000000";
                break;
            case "439":
                $RES = "3E80";
                break;
            case "5da":
                $RES = "3CFF00FFFFFFFF";
                break;
            case "218":
                $RES = "9C";
                break;
            case "4fa":
                $RES = "0000";
                break;
            case "671":
                $RES = "4000";
                break;
            case "6f8":
                $RES = "4C10DAAAFF";
                break;
            case "433":
                $RES = "00007F00";
                break;
            case "654":
                $RES = "00000015FFC1F238";
                break;
            case "68c":
                $RES = "07FF023F03FF";
                break;
            case "646":
                $RES = "408E27D98B5E2290";
                break;
            case "637":
                $RES = "00000000FC0010";
                break;
            case "62d":
                $RES = "92078000";
                break;
            case "658":
                $RES = "260046826240";
                break;
            case "69f":
                $RES = "6104488F";
                break;
            case "665":
                $RES = "00000003003F00";
                break;
            case "6fb":
                $RES = "0F03FFFE63FF00FF";
                break;
            default:
                $RES = "-E-Frame not found ";
                break;
        }

        break;

    case "n":


    default:
        break;
}

echo $ID . "," . $RES;

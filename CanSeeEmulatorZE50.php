<?php
/*
    CanSeeEmulator
    Emulates a CanSee dongle hooked to a Renault ZOE

    Copyright (C) 2020 - The CanZE Team
    http://canze.fisch.lu

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or any
    later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

$PAR = $_GET['command'];
$RES = "-";

$COMMAND = substr($PAR, 0, 1);
$ELEMENTS = explode(",", substr($PAR, 1));
$ID = $ELEMENTS[0];
switch ($COMMAND)
{

	case "i":
		if (substr($ELEMENTS[1], 0, 1) == "1")
		{
			$SID = $ID . "." . "5" . substr($ELEMENTS[1], 1);
		}
		elseif (substr($ELEMENTS[1], 0, 1) == "2")
		{
			$SID = $ID . "." . "6" . substr($ELEMENTS[1], 1);
		}
		elseif (substr($ELEMENTS[1], 0, 1) == "3")
		{
			$SID = $ID . "." . "7" . substr($ELEMENTS[1], 1);
		}
		
		switch ($SID)
		{

			case "18daf1db.629005":
				$RES = "6290050dad";
				break;
			case "18daf1da.6221cc":
				$RES = "6221cc0804";
				break;
			case "18daf1da.622247":
				$RES = "7f2231";
				break;
			case "18daf1db.629002":
				$RES = "6290020caa";
				break;
			case "18daf1da.623451":
				$RES = "7f2231";
				break;
			case "18daf1da.622006":
				$RES = "622006002ea9";
				break;
			case "18daf1db.629001":
				$RES = "62900110b1";
				break;
			case "18daf1db.629003":
				$RES = "62900323b7";
				break;
			case "18daf1db.629131":
				$RES = "62913102c0";
				break;
			case "18daf1db.629132":
				$RES = "62913202c7";
				break;
			case "18daf1db.629133":
				$RES = "62913302c0";
				break;
			case "18daf1db.629134":
				$RES = "62913402c7";
				break;
			case "18daf1db.629135":
				$RES = "62913502c0";
				break;
			case "18daf1db.629136":
				$RES = "62913602c0";
				break;
			case "18daf1db.629137":
				$RES = "62913702c0";
				break;
			case "18daf1db.629138":
				$RES = "62913802c0";
				break;
			case "18daf1db.629139":
				$RES = "62913902c7";
				break;
			case "18daf1db.62913a":
				$RES = "62913a02c0";
				break;
			case "18daf1db.62913b":
				$RES = "62913b02c0";
				break;
			case "18daf1db.62913c":
				$RES = "62913c02c0";
				break;
			case "18daf1db.629021":
				$RES = "6290210ea6";
				break;
			case "18daf1db.629022":
				$RES = "6290220e9c";
				break;
			case "18daf1db.629023":
				$RES = "6290230ea4";
				break;
			case "18daf1db.629024":
				$RES = "6290240e9d";
				break;
			case "18daf1db.629025":
				$RES = "6290250e9f";
				break;
			case "18daf1db.629026":
				$RES = "6290260ea2";
				break;
			case "18daf1db.629027":
				$RES = "6290270ea3";
				break;
			case "18daf1db.629028":
				$RES = "6290280e99";
				break;
			case "18daf1db.629029":
				$RES = "6290290ea9";
				break;
			case "18daf1db.62902a":
				$RES = "62902a0ea8";
				break;
			case "18daf1db.62902b":
				$RES = "62902b0ea1";
				break;
			case "18daf1db.62902c":
				$RES = "62902c0ea2";
				break;
			case "18daf1db.62902d":
				$RES = "62902d0e99";
				break;
			case "18daf1db.62902e":
				$RES = "62902e0ea4";
				break;
			case "18daf1db.62902f":
				$RES = "62902f0e9e";
				break;
			case "18daf1db.629030":
				$RES = "6290300e9e";
				break;
			case "18daf1db.629031":
				$RES = "6290310ea4";
				break;
			case "18daf1db.629032":
				$RES = "6290320e9c";
				break;
			case "18daf1db.629033":
				$RES = "6290330ea3";
				break;
			case "18daf1db.629034":
				$RES = "6290340ea1";
				break;
			case "18daf1db.629035":
				$RES = "6290350e9e";
				break;
			case "18daf1db.629036":
				$RES = "6290360ea3";
				break;
			case "18daf1db.629037":
				$RES = "6290370ea2";
				break;
			case "18daf1db.629038":
				$RES = "6290380e9c";
				break;
			case "18daf1db.629039":
				$RES = "6290390ea6";
				break;
			case "18daf1db.62903a":
				$RES = "62903a0e98";
				break;
			case "18daf1db.62903b":
				$RES = "62903b0ea3";
				break;
			case "18daf1db.62903c":
				$RES = "62903c0e98";
				break;
			case "18daf1db.62903d":
				$RES = "62903d0e9e";
				break;
			case "18daf1db.62903e":
				$RES = "62903e0ea7";
				break;
			case "18daf1db.62903f":
				$RES = "62903f0e9f";
				break;
			case "18daf1db.629041":
				$RES = "6290410e9e";
				break;
			case "18daf1db.629042":
				$RES = "6290420ea4";
				break;
			case "18daf1db.629043":
				$RES = "6290430ea6";
				break;
			case "18daf1db.629044":
				$RES = "6290440e99";
				break;
			case "18daf1db.629045":
				$RES = "6290450e99";
				break;
			case "18daf1db.629046":
				$RES = "6290460e9d";
				break;
			case "18daf1db.629047":
				$RES = "6290470ea7";
				break;
			case "18daf1db.629048":
				$RES = "6290480e9a";
				break;
			case "18daf1db.629049":
				$RES = "6290490e9f";
				break;
			case "18daf1db.62904a":
				$RES = "62904a0ea4";
				break;
			case "18daf1db.62904b":
				$RES = "62904b0e9c";
				break;
			case "18daf1db.62904c":
				$RES = "62904c0ea2";
				break;
			case "18daf1db.62904d":
				$RES = "62904d0ea3";
				break;
			case "18daf1db.62904e":
				$RES = "62904e0ea6";
				break;
			case "18daf1db.62904f":
				$RES = "62904f0e9a";
				break;
			case "18daf1db.629050":
				$RES = "6290500e9e";
				break;
			case "18daf1db.629051":
				$RES = "6290510ea7";
				break;
			case "18daf1db.629052":
				$RES = "6290520e9e";
				break;
			case "18daf1db.629053":
				$RES = "6290530e9f";
				break;
			case "18daf1db.629054":
				$RES = "6290540ea6";
				break;
			case "18daf1db.629055":
				$RES = "6290550e9a";
				break;
			case "18daf1db.629056":
				$RES = "6290560e9e";
				break;
			case "18daf1db.629057":
				$RES = "6290570e9a";
				break;
			case "18daf1db.629058":
				$RES = "6290580e9c";
				break;
			case "18daf1db.629059":
				$RES = "6290590ea8";
				break;
			case "18daf1db.62905a":
				$RES = "62905a0e98";
				break;
			case "18daf1db.62905b":
				$RES = "62905b0e9f";
				break;
			case "18daf1db.62905c":
				$RES = "62905c0ea7";
				break;
			case "18daf1db.62905d":
				$RES = "62905d0e9a";
				break;
			case "18daf1db.62905e":
				$RES = "62905e0e9e";
				break;
			case "18daf1db.62905f":
				$RES = "62905f0ea7";
				break;
			case "18daf1db.629061":
				$RES = "6290610e9e";
				break;
			case "18daf1db.629062":
				$RES = "6290620ea8";
				break;
			case "18daf1db.629063":
				$RES = "6290630e9c";
				break;
			case "18daf1db.629064":
				$RES = "6290640e9a";
				break;
			case "18daf1db.629065":
				$RES = "6290650ea7";
				break;
			case "18daf1db.629066":
				$RES = "6290660e9f";
				break;
			case "18daf1db.629067":
				$RES = "6290670e98";
				break;
			case "18daf1db.629068":
				$RES = "6290680ea6";
				break;
			case "18daf1db.629069":
				$RES = "6290690ea7";
				break;
			case "18daf1db.62906a":
				$RES = "62906a0e9a";
				break;
			case "18daf1db.62906b":
				$RES = "62906b0ea6";
				break;
			case "18daf1db.62906c":
				$RES = "62906c0e9d";
				break;
			case "18daf1db.62906d":
				$RES = "62906d0ea6";
				break;
			case "18daf1db.62906e":
				$RES = "62906e0ea1";
				break;
			case "18daf1db.62906f":
				$RES = "62906f0e9d";
				break;
			case "18daf1db.629070":
				$RES = "6290700ea7";
				break;
			case "18daf1db.629071":
				$RES = "6290710e99";
				break;
			case "18daf1db.629072":
				$RES = "6290720e9d";
				break;
			case "18daf1db.629073":
				$RES = "6290730eab";
				break;
			case "18daf1db.629074":
				$RES = "6290740e9f";
				break;
			case "18daf1db.629075":
				$RES = "6290750ea7";
				break;
			case "18daf1db.629076":
				$RES = "6290760e9a";
				break;
			case "18daf1db.629077":
				$RES = "6290770e9e";
				break;
			case "18daf1db.629078":
				$RES = "6290780ea4";
				break;
			case "18daf1db.629079":
				$RES = "6290790e9c";
				break;
			case "18daf1db.62907a":
				$RES = "62907a0e9a";
				break;
			case "18daf1db.62907b":
				$RES = "62907b0ea9";
				break;
			case "18daf1db.62907c":
				$RES = "62907c0e9a";
				break;
			case "18daf1db.62907d":
				$RES = "62907d0e9f";
				break;
			case "18daf1db.62907e":
				$RES = "62907e0e9a";
				break;
			case "18daf1db.62907f":
				$RES = "62907f0e9d";
				break;
			case "18daf1db.629081":
				$RES = "6290810e9a";
				break;
			case "18daf1db.629082":
				$RES = "6290820ea4";
				break;
			case "18daf1db.629083":
				$RES = "6290830e9e";
				break;
			case "764.624258":
				$RES = "62425800";
				break;
			case "764.6243d8":
				$RES = "6243d80000";
				break;
			case "764.624266":
				$RES = "62426601";
				break;
			case "764.62441c":
				$RES = "62441c00";
				break;
			case "764.624409":
				$RES = "62440907";
				break;
			case "18daf1da.622b79":
				$RES = "622b790000";
				break;
			case "764.62426c":
				$RES = "62426c001e";
				break;
			case "764.624004":
				$RES = "62400400";
				break;
			case "18daf1db.629012":
				$RES = "62901202c1";
				break;
			case "764.624347":
				$RES = "62434700e1";
				break;
			case "764.624009":
				$RES = "624009020b";
				break;
			case "764.624423":
				$RES = "62442301c2";
				break;
			case "18daf1da.5003":
				$RES = "5003003201f4";
				break;
			case "18daf1da.62300f":
				$RES = "62300f00d4";
				break;
			case "18daf1db.629018":
				$RES = "6290181439";
				break;
			case "18daf1da.62202e":
				$RES = "62202e0000";
				break;
			case "18daf1da.6233de":
				$RES = "7f2231";
				break;
			case "18daf1da.6233dd":
				$RES = "7f2231";
				break;
			case "18daf1de.50c0":
				$RES = "50c0";
				break;
			case "18daf1de.622001":
				$RES = "6220010e1f";
				break;
			case "18daf1de.62503a":
				$RES = "62503a0c81";
				break;
			case "18daf1de.625017":
				$RES = "62501701";
				break;
			case "18daf1de.62503b":
				$RES = "62503b0e1b";
				break;
			case "18daf1de.62503f":
				$RES = "62503f19f9";
				break;
			case "18daf1de.625041":
				$RES = "62504119ac";
				break;
			case "18daf1de.625042":
				$RES = "62504233fa";
				break;
			case "18daf1de.62504a":
				$RES = "62504a62f0";
				break;
			case "18daf1de.625062":
				$RES = "625062005e";
				break;
			case "18daf1de.625063":
				$RES = "62506306";
				break;
			case "18daf1de.625064":
				$RES = "62506401";
				break;
			case "18daf1da.623006":
				$RES = "6230060000";
				break;
			case "18daf1da.623008":
				$RES = "6230080000";
				break;
			case "18daf1da.62300a":
				$RES = "62300a0000";
				break;
			case "7c8.620329":
				$RES = "62032900";
				break;
			case "7c8.620326":
				$RES = "62032600";
				break;
			case "7c8.62032c":
				$RES = "62032c00";
				break;
			case "7c8.62032d":
				$RES = "62032d00";
				break;
			case "7c8.620334":
				$RES = "62033400";
				break;
			case "7c8.620336":
				$RES = "6203360000";
				break;
			case "7c8.620337":
				$RES = "62033700";
				break;
			case "7c8.620335":
				$RES = "6203350000";
				break;
			case "7c8.62033c":
				$RES = "62033c00";
				break;
			case "7c8.62033b":
				$RES = "62033b00";
				break;
			case "7c8.62033a":
				$RES = "62033a00";
				break;
			case "18daf1de.7e01":
				$RES = "7e";
				break;
			case "18daf1db.6291c8":
				$RES = "6291c80039a8";
				break;
			case "18daf1da.622b85":
				$RES = "622b8501";
				break;
			case "18daf1db.6107":
				$RES = "7f2111";
				break;
			case "18daf1da.623088":
				$RES = "62308856";
				break;
			case "18daf1db.629243":
				$RES = "629243001a557a";
				break;
			case "18daf1db.629210":
				$RES = "629210001d";
				break;
			case "18daf1db.629215":
				$RES = "62921500c1";
				break;
			case "18daf1db.629247":
				$RES = "6292470008a194";
				break;
			case "18daf1da.623085":
				$RES = "623085000000000000000000000000000000000000000000000000000000002ea9";
				break;
			case "18daf1da.6233d5":
				$RES = "7f2231";
				break;
			case "18daf1da.623084":
				$RES = "62308403000000000000000000";
				break;
			case "18daf1da.623150":
				$RES = "62315000000000000000000000000000000000000005da";
				break;
			case "18daf1da.623082":
				$RES = "6230822828282828282828282c";
				break;
			case "18daf1da.623086":
				$RES = "6230860000000000000000000000000000000000000000";
				break;
			case "18daf1da.623478":
				$RES = "7f2231";
				break;
			case "18daf1da.623457":
				$RES = "7f2231";
				break;
			case "18daf1da.623455":
				$RES = "7f2231";
				break;
			case "18daf1de.625057":
				$RES = "6250577ffc";
				break;
			case "18daf1de.62505a":
				$RES = "62505a8066";
				break;
			case "18daf1de.625059":
				$RES = "625059828e";
				break;
			case "18daf1de.625058":
				$RES = "625058815b";
				break;
			case "18daf1db.62f1a0":
				$RES = "62f1a007";
				break;
			case "18daf1db.62f18a":
				$RES = "62f18a65326361642020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020";
				break;
			case "18daf1db.62f194":
				$RES = "62f19420353231";
				break;
			case "18daf1db.62f195":
				$RES = "62f1955a4c3030";
				break;
			case "18daf1dc.62f1a0":
				$RES = "62f1a007";
				break;
			case "18daf1dc.62f18a":
				$RES = "62f18a45324341442020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020";
				break;
			case "18daf1dc.62f194":
				$RES = "62f19420353231";
				break;
			case "18daf1dc.62f195":
				$RES = "62f1955a4c3030";
				break;
			case "763.62f1a0":
				$RES = "62f1a022";
				break;
			case "763.62f18a":
				$RES = "62f18a313056697374656f6e50616c6d656c612d2d45562020202020202020202020202020202020202020202020202020202020202020202020202020202020202020";
				break;
			case "763.62f194":
				$RES = "62f19445565f5630362e3137";
				break;
			case "763.62f195":
				$RES = "62f19531393332";
				break;
			case "734.62f1a0":
				$RES = "62f1a004";
				break;
			case "734.62f18a":
				$RES = "7f2278";
				break;
			case "734.62f195":
				$RES = "62f195535730392e3034";
				break;
			case "767.62f1a0":
				$RES = "62f1a010";
				break;
			case "767.62f18a":
				$RES = "62f18a424f534348";
				break;
			case "767.62f194":
				$RES = "62f194495f50524a5f524e5f414956495f31392e3131563234";
				break;
			case "767.62f195":
				$RES = "62f195333532345f323030373238";
				break;
			case "7c8.62f1a0":
				$RES = "62f1a004";
				break;
			case "7c8.62f18a":
				$RES = "62f18a434f4e54494e454e54414c5f454e47494e454552494e475f53455256494345532020202020202020202020202020202020202020202020202020202020202020";
				break;
			case "7c8.62f194":
				$RES = "62f19432383347383630373852";
				break;
			case "7c8.62f195":
				$RES = "62f19552455831305f3075305f3433305f303032";
				break;
			case "18daf1d2.62f1a0":
				$RES = "62f1a006";
				break;
			case "18daf1d2.62f18a":
				$RES = "62f18a42343038";
				break;
			case "18daf1d2.62f194":
				$RES = "62f1944630303556543137373920202020202020202020202020202020202020202020";
				break;
			case "18daf1d2.62f195":
				$RES = "62f1955357303030362e303030332e3030303220202020202020202020202020202020";
				break;
			case "76d.6180":
				$RES = "61803738373752403431343834393952ba656560a00001000088";
				break;
			case "18daf1da.62f1a0":
				$RES = "62f1a007";
				break;
			case "18daf1da.62f18a":
				$RES = "62f18a434f4e54494e454e54414c5f454e47494e455f53595354454d000000000000000000000000000000000000000000000000000000000000000000000000000000";
				break;
			case "18daf1da.62f194":
				$RES = "62f1943041313020202020202020202020202020202020202020202020202020202020";
				break;
			case "18daf1da.62f195":
				$RES = "62f1953641303020202020202020202020202020202020202020202020202020202020";
				break;
			case "761.6180":
				$RES = "6180323131325204553246373831345200050004050801010088";
				break;
			case "735.62f195":
			case "18daf1df.6180":
				$RES = "618030323732520742475030313630520220a400917501010188";
				break;
			case "7b9.62f1a0":
				$RES = "62f1a005";
				break;
			case "7b9.62f18a":
				$RES = "62f18a434f4e54494e454e54414c";
				break;
			case "7b9.62f194":
				$RES = "62f1943134324130322020202020202020202020202020202020202020202020202020";
				break;
			case "7b9.62f195":
				$RES = "62f1953036333020202020202020202020202020202020202020202020202020202020";
				break;
			case "762.62f1a0":
				$RES = "62f1a018";
				break;
			case "762.62f18a":
				$RES = "62f18a34414734";
				break;
			case "762.62f194":
				$RES = "62f194443130305f3130362e32322e30312e3039202020202020202020202020202020";
				break;
			case "762.62f195":
				$RES = "62f1953130362e32322e30312e30392020202020202020202020202020202020202020";
				break;
			case "18daf1e2.62f1a0":
				$RES = "62f1a004";
				break;
			case "18daf1e2.62f18a":
				$RES = "62f18a424f5343482020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020";
				break;
			case "18daf1e2.62f194":
				$RES = "62f1943935393737202020202020202020202020202020202020202020202020202020";
				break;
			case "18daf1e2.62f195":
				$RES = "62f1955f63706a563171506b45656d756b4b79624c426f445441202020202020202020";
				break;
			case "7da.62f1a0":
				$RES = "62f1a030";
				break;
			case "7da.62f18a":
				$RES = "7f2278";
				break;
			case "7da.62f195":
				$RES = "62f1952020202020202020202020202020202020202020202020202020202020202020";
				break;
			case "764.62f1a0":
				$RES = "62f1a009";
				break;
			case "764.62f18a":
				$RES = "62f18a42353137";
				break;
			case "764.62f194":
				$RES = "62f194535731322e432e31332d423034372d31342e302d4630372e3030";
				break;
			case "764.62f195":
				$RES = "62f19546303043323938";
				break;
			case "765.62f1a0":
				$RES = "62f1a090";
				break;
			case "765.62f18a":
				$RES = "62f18a434f4e54494e454e54414c204155544f4d4f54495645";
				break;
			case "765.62f194":
				$RES = "62f1943134323930322020202020202020202020202020202020202020202020202020";
				break;
			case "765.62f195":
				$RES = "62f1953037353020202020202020202020202020202020202020202020202020202020";
				break;
			case "18daf1de.6180":
				$RES = "61803031363652074a4632303131395202100a30087201010188";
				break;
			case "775.62f1a0":
				$RES = "62f1a004";
				break;
			case "775.62f18a":
				$RES = "62f18a5452572020204361726c2d537061657465722d5374722e20382020203536303730204b6f626c656e7a2020204765726d616e7920202020202020202020202020";
				break;
			case "775.62f194":
				$RES = "62f1945330315f30322020202020202020202020202020202020202020202020202020";
				break;
			case "775.62f195":
				$RES = "62f1953030315f30303045304330345331303220202020202020202020202020202020";
				break;
			case "772.6180":
				$RES = "7f2111";
				break;
			case "18daf12d.62f1a0":
				$RES = "62f1a004";
				break;
			case "18daf12d.62f18a":
				$RES = "62f18a424f5343482020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020";
				break;
			case "18daf12d.62f194":
				$RES = "62f1943935363331202020202020202020202020202020202020202020202020202020";
				break;
			case "18daf12d.62f195":
				$RES = "62f1955f483074477135353145656d756b4b79624c426f445441202020202020202020";
				break;
			case "765.628003":
				$RES = "62800305";
				break;
			case "18daf1da.622005":
				$RES = "62200505b5";
				break;
			case "18daf1da.6221df":
				$RES = "6221df800b";
				break;
			case "18daf1da.623486":
				$RES = "7f2231";
				break;
			case "18daf1da.6229fa":
				$RES = "6229fa0e";
				break;
			case "7dd.5003":
				$RES = "5003003201f4";
				break;
			case "7dd.620105":
				$RES = "7f2231";
				break;
			case "7dd.62010c":
				$RES = "62010c01";
				break;
			case "7dd.62010d":
				$RES = "62010d01";
				break;
			case "7dd.62010e":
				$RES = "62010e01";
				break;
			case "7dd.62010f":
				$RES = "62010f00";
				break;
			case "7dd.620113":
				$RES = "6201130000";
				break;
			case "7dd.620114":
				$RES = "6201140000";
				break;
			case "7dd.620115":
				$RES = "6201150000";
				break;
			case "7dd.620116":
				$RES = "620116040d";
				break;
			case "7dd.620117":
				$RES = "620117fee2";
				break;
			case "7dd.620118":
				$RES = "620118f9ec";
				break;
			case "7dd.621000":
				$RES = "621000000000";
				break;
			case "7dd.621001":
				$RES = "62100101";
				break;
			case "7dd.621002":
				$RES = "62100200";
				break;
			case "7dd.622010":
				$RES = "62201000";
				break;
			case "7dd.622012":
				$RES = "62201201";
				break;
			case "7dd.622013":
				$RES = "6220132d01";
				break;
			case "7dd.622044":
				$RES = "6220440f";
				break;
			case "7dd.622057":
				$RES = "622057256c";
				break;
			case "7dd.622059":
				$RES = "62205901";
				break;
			case "7dd.622060":
				$RES = "62206001";
				break;
			case "7dd.622061":
				$RES = "62206100";
				break;
			case "7dd.622062":
				$RES = "62206200";
				break;
			case "7dd.622063":
				$RES = "62206300";
				break;
			case "7dd.622064":
				$RES = "62206400";
				break;
			case "7dd.622065":
				$RES = "62206500";
				break;
			case "7dd.622068":
				$RES = "62206802";
				break;
			case "7dd.622069":
				$RES = "62206900";
				break;
			case "7dd.622070":
				$RES = "6220700200";
				break;
			case "7dd.622074":
				$RES = "6220740001";
				break;
			case "7dd.622079":
				$RES = "62207901";
				break;
			case "7dd.622080":
				$RES = "62208001";
				break;
			case "7dd.622081":
				$RES = "622081741d0741d003020068832140f0";
				break;
			case "7dd.622084":
				$RES = "62208400";
				break;
			case "7dd.622086":
				$RES = "62208600";
				break;
			case "7dd.622093":
				$RES = "62209301";
				break;
			case "7dd.622094":
				$RES = "62209400";
				break;
			case "7dd.622095":
				$RES = "62209500";
				break;
			case "7dd.622096":
				$RES = "62209602";
				break;
			case "7dd.622097":
				$RES = "62209702";
				break;
			case "7dd.622098":
				$RES = "62209800";
				break;
			case "7dd.622109":
				$RES = "62210962";
				break;
			case "7dd.62210a":
				$RES = "62210a02ca";
				break;
			case "7dd.62210b":
				$RES = "62210b00";
				break;
			case "7dd.62210c":
				$RES = "62210c000a";
				break;
			case "7dd.62210d":
				$RES = "62210d021b";
				break;
			case "7dd.62210e":
				$RES = "62210e3200";
				break;
			case "7dd.62210f":
				$RES = "62210f0564";
				break;
			case "7dd.622201":
				$RES = "62220100";
				break;
			case "7dd.622202":
				$RES = "62220200";
				break;
			case "7dd.622203":
				$RES = "62220300";
				break;
			case "7dd.622204":
				$RES = "62220400";
				break;
			case "7dd.622205":
				$RES = "62220500";
				break;
			case "7dd.622206":
				$RES = "62220600";
				break;
			case "7dd.622207":
				$RES = "62220700";
				break;
			case "7dd.622208":
				$RES = "62220800";
				break;
			case "7dd.622209":
				$RES = "62220900";
				break;
			case "7dd.622210":
				$RES = "62221000";
				break;
			case "7dd.622211":
				$RES = "62221100";
				break;
			case "7dd.622302":
				$RES = "6223023a9c";
				break;
			case "7dd.622303":
				$RES = "62230303dc";
				break;
			case "7dd.622304":
				$RES = "6223040000";
				break;
			case "7dd.622305":
				$RES = "6223050000";
				break;
			case "7dd.622306":
				$RES = "6223060000";
				break;
			case "7dd.622400":
				$RES = "62240000000741";
				break;
			case "7dd.622401":
				$RES = "62240100";
				break;
			case "7dd.622402":
				$RES = "6224020000";
				break;
			case "7dd.622403":
				$RES = "6224030000";
				break;
			case "7dd.622404":
				$RES = "62240400";
				break;
			case "7dd.622405":
				$RES = "6224050000";
				break;
			case "7dd.622406":
				$RES = "62240600";
				break;
			case "7dd.622407":
				$RES = "62240700";
				break;
			case "7dd.622408":
				$RES = "62240800";
				break;
			case "7dd.622409":
				$RES = "62240900";
				break;
			case "7dd.622410":
				$RES = "62241000";
				break;
			case "7dd.622411":
				$RES = "62241100";
				break;
			case "7dd.622412":
				$RES = "62241200";
				break;
			case "7dd.622413":
				$RES = "62241300";
				break;
			case "7dd.622414":
				$RES = "62241400";
				break;
			case "7dd.622415":
				$RES = "62241500";
				break;
			case "7dd.622416":
				$RES = "62241600";
				break;
			case "7dd.622417":
				$RES = "62241700";
				break;
			case "7dd.622418":
				$RES = "62241800";
				break;
			case "7dd.622419":
				$RES = "62241900";
				break;
			case "7dd.622421":
				$RES = "62242100";
				break;
			case "7dd.622422":
				$RES = "62242200";
				break;
			case "7dd.622500":
				$RES = "62250000";
				break;
			case "7dd.624008":
				$RES = "62400850";
				break;
			case "7dd.624011":
				$RES = "62401101";
				break;
			case "7dd.624012":
				$RES = "62401201";
				break;
			case "7dd.624014":
				$RES = "62401401000800";
				break;
			case "7dd.624015":
				$RES = "6240158080";
				break;
			case "7dd.624016":
				$RES = "62401601";
				break;
			case "7dd.624017":
				$RES = "62401701";
				break;
			case "7dd.624018":
				$RES = "6240180505323201";
				break;
			case "7dd.624019":
				$RES = "62401900";
				break;
			case "7dd.624020":
				$RES = "62402001";
				break;
			case "7dd.624021":
				$RES = "624021000002060000fee20100";
				break;
			case "7dd.624022":
				$RES = "62402201df348c780a08";
				break;
			case "7dd.624023":
				$RES = "6240230114";
				break;
			case "7dd.624025":
				$RES = "624025003205190032051900320519003205190032051900320519003205191388300c0bb8300c";
				break;
			case "7dd.624026":
				$RES = "62402613";
				break;
			case "7dd.624027":
				$RES = "6240274e20177001f427100bb805dc000a";
				break;
			case "7dd.624028":
				$RES = "624028001e0014";
				break;
			case "7dd.624029":
				$RES = "6240290a";
				break;
			case "7dd.624030":
				$RES = "62403001e01e01407280";
				break;
			case "7dd.624031":
				$RES = "62403180";
				break;
			case "7dd.624072":
				$RES = "62407200";
				break;
			case "7dd.624073":
				$RES = "624073000000000000000000000000";
				break;
			case "7dd.624074":
				$RES = "624074000000000000000000000000";
				break;
			case "7dd.624075":
				$RES = "624075000000000000000000000000";
				break;
			case "7dd.624076":
				$RES = "624076000000000000000000000000";
				break;
			case "7dd.624077":
				$RES = "624077000000000000000000000000";
				break;
			case "7dd.624078":
				$RES = "624078000000000000000000000000";
				break;
			case "7dd.624079":
				$RES = "624079000000000000000000000000";
				break;
			case "7dd.62407a":
				$RES = "62407a000000000000000000000000";
				break;
			case "7dd.62407b":
				$RES = "62407b000000000000000000000000";
				break;
			case "7dd.62407c":
				$RES = "62407c000000000000000000000000";
				break;
			case "7dd.62407d":
				$RES = "62407d000000000000000000000000";
				break;
			case "7dd.62407e":
				$RES = "62407e000000000000000000000000";
				break;
			case "7dd.62407f":
				$RES = "62407f000000000000000000000000";
				break;
			case "7dd.624080":
				$RES = "624080000000000000000000000000";
				break;
			case "7dd.624081":
				$RES = "624081000000000000000000000000";
				break;
			case "7dd.624082":
				$RES = "624082000000000000000000000000";
				break;
			case "7dd.624083":
				$RES = "624083000000000000000000000000";
				break;
			case "7dd.624084":
				$RES = "624084000000000000000000000000";
				break;
			case "7dd.624085":
				$RES = "624085000000000000000000000000";
				break;
			case "7dd.624086":
				$RES = "624086000000000000000000000000";
				break;
			case "7dd.624087":
				$RES = "624087000000000000";
				break;
			case "7dd.624088":
				$RES = "624088000000000000";
				break;
			case "7dd.624089":
				$RES = "624089000000000000";
				break;
			case "7dd.62408a":
				$RES = "62408a000000000000";
				break;
			case "7dd.62408b":
				$RES = "62408b000000000000";
				break;
			case "7dd.62408c":
				$RES = "62408c000000000000";
				break;
			case "7dd.62408d":
				$RES = "62408d000000000000";
				break;
			case "7dd.62408e":
				$RES = "62408e000000000000";
				break;
			case "7dd.62408f":
				$RES = "62408f000000000000";
				break;
			case "7dd.624090":
				$RES = "624090000000000000";
				break;
			case "7dd.624091":
				$RES = "624091000000000000";
				break;
			case "7dd.624092":
				$RES = "624092000000000000";
				break;
			case "7dd.624093":
				$RES = "624093000000000000";
				break;
			case "7dd.624094":
				$RES = "624094000000000000";
				break;
			case "7dd.624095":
				$RES = "624095000000000000";
				break;
			case "7dd.624096":
				$RES = "624096000000000000";
				break;
			case "7dd.624097":
				$RES = "624097000000000000";
				break;
			case "7dd.624098":
				$RES = "624098000000000000";
				break;
			case "7dd.624099":
				$RES = "624099000000000000";
				break;
			case "7dd.62409a":
				$RES = "62409a000000000000";
				break;
			case "7dd.624455":
				$RES = "7f2231";
				break;
			case "7dd.624456":
				$RES = "7f2231";
				break;
			case "7dd.624457":
				$RES = "7f2231";
				break;
			case "7dd.62d900":
				$RES = "7f2231";
				break;
			case "7dd.62d90a":
				$RES = "62d90a03e8";
				break;
			case "7dd.62d90c":
				$RES = "62d90c0000";
				break;
			case "7dd.62d915":
				$RES = "62d9150000";
				break;
			case "7dd.62d922":
				$RES = "62d9220000";
				break;
			case "7dd.62f010":
				$RES = "62f01020202020202020202020202020202020";
				break;
			case "7dd.62f011":
				$RES = "62f0115256414c454f4643414d";
				break;
			case "7dd.62f012":
				$RES = "62f01232383441363934343852";
				break;
			case "7dd.62f0d0":
				$RES = "62f0d000000000";
				break;
			case "7dd.62f0d1":
				$RES = "62f0d100";
				break;
			case "7dd.62f182":
				$RES = "62f18201f596f98c7d47bb1def";
				break;
			case "7dd.62f187":
				$RES = "62f18732383436323937363552";
				break;
			case "7dd.62f188":
				$RES = "62f18832383441363335373652";
				break;
			case "7dd.62f18c":
				$RES = "62f18c57454d303130312d323031393132313131363136";
				break;
			case "7dd.62f18e":
				$RES = "7f2231";
				break;
			case "7dd.62f190":
				$RES = "62f1905646314147303030303634393531353432";
				break;
			case "7dd.62f191":
				$RES = "62f1913238354a393139303552";
				break;
			case "7dd.62f192":
				$RES = "7f2231";
				break;
			case "7dd.62fd01":
				$RES = "7f2231";
				break;
			case "7dd.7e00":
				$RES = "7e00";
				break;
			case "18daf1db.5003":
				$RES = "5003003201f4";
				break;
			case "18daf1db.629000":
				$RES = "6290003f1ef7ee";
				break;
			case "18daf1db.629006":
				$RES = "62900600057de0";
				break;
			case "18daf1db.629007":
				$RES = "6290070e9e";
				break;
			case "18daf1db.629008":
				$RES = "62900814";
				break;
			case "18daf1db.629009":
				$RES = "6290090e9e";
				break;
			case "18daf1db.62900a":
				$RES = "62900a0c";
				break;
			case "18daf1db.62900c":
				$RES = "62900c7f7b";
				break;
			case "18daf1db.62900d":
				$RES = "62900d0000bd85";
				break;
			case "18daf1db.62900e":
				$RES = "62900e0ce4";
				break;
			case "18daf1db.62900f":
				$RES = "62900f28b2";
				break;
			case "18daf1db.629011":
				$RES = "6290113861";
				break;
			case "18daf1db.629013":
				$RES = "62901302c0";
				break;
			case "18daf1db.629014":
				$RES = "62901402c7";
				break;
			case "18daf1db.629019":
				$RES = "62901900";
				break;
			case "18daf1db.62901a":
				$RES = "62901a02";
				break;
			case "18daf1db.62901b":
				$RES = "62901b32393531303539323852544630333030313346464646464646";
				break;
			case "18daf1db.62901c":
				$RES = "62901c524c3046324b4333303031334b43394152303030";
				break;
			case "18daf1db.62901d":
				$RES = "62901d3201f446324b4333";
				break;
			case "18daf1db.629020":
				$RES = "629020fffffffe";
				break;
			case "18daf1db.629040":
				$RES = "629040fffffffe";
				break;
			case "18daf1db.629060":
				$RES = "629060fffffffe";
				break;
			case "18daf1db.629080":
				$RES = "6290800000000e";
				break;
			case "18daf1db.629084":
				$RES = "7f2231";
				break;
			case "18daf1db.629085":
				$RES = "7f2231";
				break;
			case "18daf1db.629086":
				$RES = "7f2231";
				break;
			case "18daf1db.629087":
				$RES = "7f2231";
				break;
			case "18daf1db.629088":
				$RES = "7f2231";
				break;
			case "18daf1db.629089":
				$RES = "7f2231";
				break;
			case "18daf1db.62908a":
				$RES = "7f2231";
				break;
			case "18daf1db.62908b":
				$RES = "7f2231";
				break;
			case "18daf1db.62908c":
				$RES = "7f2231";
				break;
			case "18daf1db.62908d":
				$RES = "7f2231";
				break;
			case "18daf1db.62908e":
				$RES = "7f2231";
				break;
			case "18daf1db.62908f":
				$RES = "7f2231";
				break;
			case "18daf1db.629090":
				$RES = "7f2231";
				break;
			case "18daf1db.629091":
				$RES = "7f2231";
				break;
			case "18daf1db.629092":
				$RES = "7f2231";
				break;
			case "18daf1db.629093":
				$RES = "7f2231";
				break;
			case "18daf1db.629094":
				$RES = "7f2231";
				break;
			case "18daf1db.629095":
				$RES = "7f2231";
				break;
			case "18daf1db.629096":
				$RES = "7f2231";
				break;
			case "18daf1db.629097":
				$RES = "7f2231";
				break;
			case "18daf1db.629098":
				$RES = "7f2231";
				break;
			case "18daf1db.629099":
				$RES = "7f2231";
				break;
			case "18daf1db.62909a":
				$RES = "7f2231";
				break;
			case "18daf1db.62909b":
				$RES = "7f2231";
				break;
			case "18daf1db.62909c":
				$RES = "7f2231";
				break;
			case "18daf1db.62909d":
				$RES = "7f2231";
				break;
			case "18daf1db.62909e":
				$RES = "7f2231";
				break;
			case "18daf1db.62909f":
				$RES = "7f2231";
				break;
			case "18daf1db.6290a0":
				$RES = "7f2231";
				break;
			case "18daf1db.6290a1":
				$RES = "7f2231";
				break;
			case "18daf1db.6290a2":
				$RES = "7f2231";
				break;
			case "18daf1db.6290a3":
				$RES = "7f2231";
				break;
			case "18daf1db.6290a4":
				$RES = "7f2231";
				break;
			case "18daf1db.6290a5":
				$RES = "7f2231";
				break;
			case "18daf1db.6290a6":
				$RES = "7f2231";
				break;
			case "18daf1db.6290a7":
				$RES = "7f2231";
				break;
			case "18daf1db.6290a8":
				$RES = "7f2231";
				break;
			case "18daf1db.6290a9":
				$RES = "7f2231";
				break;
			case "18daf1db.6290aa":
				$RES = "7f2231";
				break;
			case "18daf1db.6290ab":
				$RES = "7f2231";
				break;
			case "18daf1db.6290ac":
				$RES = "7f2231";
				break;
			case "18daf1db.6290ad":
				$RES = "7f2231";
				break;
			case "18daf1db.6290ae":
				$RES = "7f2231";
				break;
			case "18daf1db.6290af":
				$RES = "7f2231";
				break;
			case "18daf1db.6290b0":
				$RES = "7f2231";
				break;
			case "18daf1db.6290b1":
				$RES = "7f2231";
				break;
			case "18daf1db.6290b2":
				$RES = "7f2231";
				break;
			case "18daf1db.6290b3":
				$RES = "7f2231";
				break;
			case "18daf1db.6290b4":
				$RES = "7f2231";
				break;
			case "18daf1db.6290b5":
				$RES = "7f2231";
				break;
			case "18daf1db.6290b6":
				$RES = "7f2231";
				break;
			case "18daf1db.6290b7":
				$RES = "7f2231";
				break;
			case "18daf1db.6290b8":
				$RES = "7f2231";
				break;
			case "18daf1db.6290b9":
				$RES = "7f2231";
				break;
			case "18daf1db.6290ba":
				$RES = "7f2231";
				break;
			case "18daf1db.6290bb":
				$RES = "7f2231";
				break;
			case "18daf1db.6290bc":
				$RES = "7f2231";
				break;
			case "18daf1db.6290bd":
				$RES = "7f2231";
				break;
			case "18daf1db.6290be":
				$RES = "7f2231";
				break;
			case "18daf1db.6290bf":
				$RES = "7f2231";
				break;
			case "18daf1db.6290c0":
				$RES = "7f2231";
				break;
			case "18daf1db.6290c1":
				$RES = "7f2231";
				break;
			case "18daf1db.6290c2":
				$RES = "7f2231";
				break;
			case "18daf1db.6290c3":
				$RES = "7f2231";
				break;
			case "18daf1db.6290c4":
				$RES = "7f2231";
				break;
			case "18daf1db.6290c5":
				$RES = "7f2231";
				break;
			case "18daf1db.6290c6":
				$RES = "7f2231";
				break;
			case "18daf1db.6290c7":
				$RES = "7f2231";
				break;
			case "18daf1db.6290c8":
				$RES = "7f2231";
				break;
			case "18daf1db.6290c9":
				$RES = "7f2231";
				break;
			case "18daf1db.6290ca":
				$RES = "7f2231";
				break;
			case "18daf1db.6290cb":
				$RES = "7f2231";
				break;
			case "18daf1db.6290cc":
				$RES = "7f2231";
				break;
			case "18daf1db.6290cd":
				$RES = "7f2231";
				break;
			case "18daf1db.6290ce":
				$RES = "7f2231";
				break;
			case "18daf1db.6290cf":
				$RES = "7f2231";
				break;
			case "18daf1db.6290d0":
				$RES = "7f2231";
				break;
			case "18daf1db.6290d1":
				$RES = "7f2231";
				break;
			case "18daf1db.6290d2":
				$RES = "7f2231";
				break;
			case "18daf1db.6290d3":
				$RES = "7f2231";
				break;
			case "18daf1db.6290d4":
				$RES = "7f2231";
				break;
			case "18daf1db.6290d5":
				$RES = "7f2231";
				break;
			case "18daf1db.6290d6":
				$RES = "7f2231";
				break;
			case "18daf1db.6290d7":
				$RES = "7f2231";
				break;
			case "18daf1db.6290d8":
				$RES = "7f2231";
				break;
			case "18daf1db.6290d9":
				$RES = "7f2231";
				break;
			case "18daf1db.6290da":
				$RES = "7f2231";
				break;
			case "18daf1db.6290db":
				$RES = "7f2231";
				break;
			case "18daf1db.6290dc":
				$RES = "7f2231";
				break;
			case "18daf1db.6290dd":
				$RES = "7f2231";
				break;
			case "18daf1db.6290de":
				$RES = "7f2231";
				break;
			case "18daf1db.6290df":
				$RES = "7f2231";
				break;
			case "18daf1db.6290e0":
				$RES = "7f2231";
				break;
			case "18daf1db.6290e1":
				$RES = "7f2231";
				break;
			case "18daf1db.6290e2":
				$RES = "7f2231";
				break;
			case "18daf1db.6290e3":
				$RES = "7f2231";
				break;
			case "18daf1db.6290e4":
				$RES = "7f2231";
				break;
			case "18daf1db.6290e5":
				$RES = "7f2231";
				break;
			case "18daf1db.6290e6":
				$RES = "7f2231";
				break;
			case "18daf1db.6290e7":
				$RES = "7f2231";
				break;
			case "18daf1db.6290e8":
				$RES = "7f2231";
				break;
			case "18daf1db.6290e9":
				$RES = "7f2231";
				break;
			case "18daf1db.6290ea":
				$RES = "7f2231";
				break;
			case "18daf1db.6290eb":
				$RES = "7f2231";
				break;
			case "18daf1db.6290ec":
				$RES = "7f2231";
				break;
			case "18daf1db.6290ed":
				$RES = "7f2231";
				break;
			case "18daf1db.6290ee":
				$RES = "7f2231";
				break;
			case "18daf1db.6290ef":
				$RES = "7f2231";
				break;
			case "18daf1db.6290f0":
				$RES = "7f2231";
				break;
			case "18daf1db.6290f1":
				$RES = "7f2231";
				break;
			case "18daf1db.6290f2":
				$RES = "7f2231";
				break;
			case "18daf1db.6290f3":
				$RES = "7f2231";
				break;
			case "18daf1db.6290f4":
				$RES = "7f2231";
				break;
			case "18daf1db.6290f5":
				$RES = "7f2231";
				break;
			case "18daf1db.6290f6":
				$RES = "7f2231";
				break;
			case "18daf1db.6290f7":
				$RES = "7f2231";
				break;
			case "18daf1db.6290f8":
				$RES = "7f2231";
				break;
			case "18daf1db.6290f9":
				$RES = "7f2231";
				break;
			case "18daf1db.6290fa":
				$RES = "7f2231";
				break;
			case "18daf1db.6290fb":
				$RES = "7f2231";
				break;
			case "18daf1db.6290fc":
				$RES = "7f2231";
				break;
			case "18daf1db.6290fd":
				$RES = "7f2231";
				break;
			case "18daf1db.6290fe":
				$RES = "7f2231";
				break;
			case "18daf1db.6290ff":
				$RES = "7f2231";
				break;
			case "18daf1db.629100":
				$RES = "7f2231";
				break;
			case "18daf1db.629101":
				$RES = "7f2231";
				break;
			case "18daf1db.629102":
				$RES = "7f2231";
				break;
			case "18daf1db.629103":
				$RES = "7f2231";
				break;
			case "18daf1db.629104":
				$RES = "7f2231";
				break;
			case "18daf1db.629105":
				$RES = "7f2231";
				break;
			case "18daf1db.629106":
				$RES = "7f2231";
				break;
			case "18daf1db.629107":
				$RES = "7f2231";
				break;
			case "18daf1db.629108":
				$RES = "7f2231";
				break;
			case "18daf1db.629109":
				$RES = "7f2231";
				break;
			case "18daf1db.62910a":
				$RES = "7f2231";
				break;
			case "18daf1db.62910b":
				$RES = "7f2231";
				break;
			case "18daf1db.62910c":
				$RES = "7f2231";
				break;
			case "18daf1db.62910d":
				$RES = "7f2231";
				break;
			case "18daf1db.62910e":
				$RES = "7f2231";
				break;
			case "18daf1db.62910f":
				$RES = "7f2231";
				break;
			case "18daf1db.629110":
				$RES = "7f2231";
				break;
			case "18daf1db.629111":
				$RES = "7f2231";
				break;
			case "18daf1db.629112":
				$RES = "7f2231";
				break;
			case "18daf1db.629113":
				$RES = "7f2231";
				break;
			case "18daf1db.629114":
				$RES = "7f2231";
				break;
			case "18daf1db.629115":
				$RES = "7f2231";
				break;
			case "18daf1db.629116":
				$RES = "7f2231";
				break;
			case "18daf1db.629117":
				$RES = "7f2231";
				break;
			case "18daf1db.629118":
				$RES = "7f2231";
				break;
			case "18daf1db.629119":
				$RES = "7f2231";
				break;
			case "18daf1db.62911a":
				$RES = "7f2231";
				break;
			case "18daf1db.62911b":
				$RES = "7f2231";
				break;
			case "18daf1db.62911c":
				$RES = "7f2231";
				break;
			case "18daf1db.62911d":
				$RES = "7f2231";
				break;
			case "18daf1db.62911e":
				$RES = "7f2231";
				break;
			case "18daf1db.62911f":
				$RES = "7f2231";
				break;
			case "18daf1db.629120":
				$RES = "629120ffffca00";
				break;
			case "18daf1db.629121":
				$RES = "7f2231";
				break;
			case "18daf1db.629122":
				$RES = "7f2231";
				break;
			case "18daf1db.629123":
				$RES = "7f2231";
				break;
			case "18daf1db.629124":
				$RES = "7f2231";
				break;
			case "18daf1db.629125":
				$RES = "7f2231";
				break;
			case "18daf1db.629126":
				$RES = "7f2231";
				break;
			case "18daf1db.629127":
				$RES = "7f2231";
				break;
			case "18daf1db.629128":
				$RES = "7f2231";
				break;
			case "18daf1db.629129":
				$RES = "62912900000000324b4333303031334b4339415230303046464646462020202020202020202020202020202020202020202020202020202020202020202020202020209056014090503b2090503b00904a75209044b200";
				break;
			case "18daf1db.62912b":
				$RES = "62912b0000000000000000000000000000000000000000000000000000000000000000";
				break;
			case "18daf1db.62912e":
				$RES = "62912eff";
				break;
			case "18daf1db.62912f":
				$RES = "62912f000000000000";
				break;
			case "18daf1db.629130":
				$RES = "629130340b";
				break;
			case "18daf1db.62913d":
				$RES = "62913d0280";
				break;
			case "18daf1db.62913e":
				$RES = "62913e0280";
				break;
			case "18daf1db.62913f":
				$RES = "62913f0280";
				break;
			case "18daf1db.629140":
				$RES = "62914000000006";
				break;
			case "18daf1db.629141":
				$RES = "6291410280";
				break;
			case "18daf1db.629142":
				$RES = "6291420280";
				break;
			case "18daf1db.629160":
				$RES = "629160fff80000";
				break;
			case "18daf1db.629173":
				$RES = "629173082f";
				break;
			case "18daf1db.629174":
				$RES = "6291740830";
				break;
			case "18daf1db.629175":
				$RES = "62917524";
				break;
			case "18daf1db.629176":
				$RES = "62917624";
				break;
			case "18daf1db.629177":
				$RES = "62917724";
				break;
			case "18daf1db.629178":
				$RES = "62917824";
				break;
			case "18daf1db.629179":
				$RES = "62917924";
				break;
			case "18daf1db.62917a":
				$RES = "62917a24";
				break;
			case "18daf1db.62917b":
				$RES = "62917b24";
				break;
			case "18daf1db.62917c":
				$RES = "62917c24";
				break;
			case "18daf1db.62917d":
				$RES = "62917d24";
				break;
			case "18daf1db.62917e":
				$RES = "62917e24";
				break;
			case "18daf1db.62917f":
				$RES = "62917f24";
				break;
			case "18daf1db.629180":
				$RES = "6291800000003e";
				break;
			case "18daf1db.629181":
				$RES = "62918100";
				break;
			case "18daf1db.629182":
				$RES = "62918200";
				break;
			case "18daf1db.629183":
				$RES = "62918300";
				break;
			case "18daf1db.629184":
				$RES = "62918400";
				break;
			case "18daf1db.629185":
				$RES = "62918500";
				break;
			case "18daf1db.6291a0":
				$RES = "6291a076800000";
				break;
			case "18daf1db.6291b7":
				$RES = "6291b703a0";
				break;
			case "18daf1db.6291b9":
				$RES = "6291b91093";
				break;
			case "18daf1db.6291ba":
				$RES = "6291ba115b";
				break;
			case "18daf1db.6291bc":
				$RES = "6291bc03e8";
				break;
			case "18daf1db.6291bd":
				$RES = "6291bd03e8";
				break;
			case "18daf1db.6291be":
				$RES = "6291be0417";
				break;
			case "18daf1db.6291c0":
				$RES = "6291c08007bfc2";
				break;
			case "18daf1db.6291c1":
				$RES = "6291c1092a9f";
				break;
			case "18daf1db.6291c6":
				$RES = "6291c60000";
				break;
			case "18daf1db.6291c7":
				$RES = "6291c70000";
				break;
			case "18daf1db.6291c9":
				$RES = "6291c900";
				break;
			case "18daf1db.6291ca":
				$RES = "6291ca00";
				break;
			case "18daf1db.6291cb":
				$RES = "6291cb00";
				break;
			case "18daf1db.6291cc":
				$RES = "6291cc000000c20000000000000000000000000000000000000000000000000000000020202020202020202020202020202020202020202020202020202020202020209056014090503b2090503b00904a75209044b200";
				break;
			case "18daf1db.6291cd":
				$RES = "6291cd0000";
				break;
			case "18daf1db.6291cf":
				$RES = "6291cf90b2f70e";
				break;
			case "18daf1db.6291d0":
				$RES = "6291d001";
				break;
			case "18daf1db.6291d1":
				$RES = "6291d10000000000000000000000030013001f0004000000c001f403ce002100000067012602a70032000000000000000000000000";
				break;
			case "18daf1db.6291d2":
				$RES = "6291d2000000000000000000000a0c15282592032c000045628be6ffff117900002eae83f1ffff1267000000000000000000000000";
				break;
			case "18daf1db.6291df":
				$RES = "6291df01";
				break;
			case "18daf1db.6291e0":
				$RES = "6291e083ffffe0";
				break;
			case "18daf1db.6291e1":
				$RES = "7f2231";
				break;
			case "18daf1db.6291e2":
				$RES = "7f2231";
				break;
			case "18daf1db.6291e3":
				$RES = "7f2231";
				break;
			case "18daf1db.6291e4":
				$RES = "7f2231";
				break;
			case "18daf1db.6291e5":
				$RES = "6291e500";
				break;
			case "18daf1db.6291e6":
				$RES = "6291e600";
				break;
			case "18daf1db.6291e7":
				$RES = "6291e700";
				break;
			case "18daf1db.6291e8":
				$RES = "6291e800";
				break;
			case "18daf1db.6291e9":
				$RES = "6291e900";
				break;
			case "18daf1db.6291ea":
				$RES = "6291ea00";
				break;
			case "18daf1db.6291eb":
				$RES = "6291eb00";
				break;
			case "18daf1db.6291ec":
				$RES = "6291ec00";
				break;
			case "18daf1db.6291ed":
				$RES = "6291ed00";
				break;
			case "18daf1db.6291ee":
				$RES = "6291ee00";
				break;
			case "18daf1db.6291ef":
				$RES = "6291ef00";
				break;
			case "18daf1db.6291f0":
				$RES = "6291f000";
				break;
			case "18daf1db.6291f1":
				$RES = "6291f100";
				break;
			case "18daf1db.6291f2":
				$RES = "6291f20000";
				break;
			case "18daf1db.6291f3":
				$RES = "6291f301";
				break;
			case "18daf1db.6291f4":
				$RES = "6291f403e8";
				break;
			case "18daf1db.6291f5":
				$RES = "6291f503";
				break;
			case "18daf1db.6291f6":
				$RES = "6291f601";
				break;
			case "18daf1db.6291f7":
				$RES = "6291f701";
				break;
			case "18daf1db.6291f8":
				$RES = "6291f800";
				break;
			case "18daf1db.6291f9":
				$RES = "6291f900";
				break;
			case "18daf1db.6291ff":
				$RES = "6291ff0000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000";
				break;
			case "18daf1db.629200":
				$RES = "6292007fe10000";
				break;
			case "18daf1db.629211":
				$RES = "7f2231";
				break;
			case "18daf1db.629212":
				$RES = "7f2231";
				break;
			case "18daf1db.629213":
				$RES = "7f2231";
				break;
			case "18daf1db.629214":
				$RES = "7f2231";
				break;
			case "18daf1db.629216":
				$RES = "6292160a01f40000000000";
				break;
			case "18daf1db.629217":
				$RES = "62921700";
				break;
			case "18daf1db.629218":
				$RES = "62921800";
				break;
			case "18daf1db.629219":
				$RES = "62921901";
				break;
			case "18daf1db.62921a":
				$RES = "62921a00";
				break;
			case "18daf1db.62921b":
				$RES = "62921b00";
				break;
			case "18daf1db.62921c":
				$RES = "62921c0140";
				break;
			case "18daf1db.62921d":
				$RES = "62921d00";
				break;
			case "18daf1db.62921e":
				$RES = "62921e64";
				break;
			case "18daf1db.629220":
				$RES = "629220000001fe";
				break;
			case "18daf1db.629221":
				$RES = "6292210000";
				break;
			case "18daf1db.629222":
				$RES = "6292220008";
				break;
			case "18daf1db.629223":
				$RES = "6292230003";
				break;
			case "18daf1db.629224":
				$RES = "629224fff6";
				break;
			case "18daf1db.629225":
				$RES = "6292250003";
				break;
			case "18daf1db.629226":
				$RES = "6292260000";
				break;
			case "18daf1db.629227":
				$RES = "629227fffc";
				break;
			case "18daf1db.629228":
				$RES = "6292280001";
				break;
			case "18daf1db.629229":
				$RES = "7f2231";
				break;
			case "18daf1db.62922a":
				$RES = "7f2231";
				break;
			case "18daf1db.62922b":
				$RES = "7f2231";
				break;
			case "18daf1db.62922c":
				$RES = "7f2231";
				break;
			case "18daf1db.629240":
				$RES = "629240fff7dffe";
				break;
			case "18daf1db.629241":
				$RES = "62924101";
				break;
			case "18daf1db.629242":
				$RES = "62924205";
				break;
			case "18daf1db.629244":
				$RES = "62924400000000";
				break;
			case "18daf1db.629245":
				$RES = "6292450021a42e";
				break;
			case "18daf1db.629246":
				$RES = "62924600000000";
				break;
			case "18daf1db.629248":
				$RES = "62924801be";
				break;
			case "18daf1db.629249":
				$RES = "62924901be";
				break;
			case "18daf1db.62924a":
				$RES = "62924a23b7";
				break;
			case "18daf1db.62924b":
				$RES = "62924b23b7";
				break;
			case "18daf1db.62924c":
				$RES = "62924c23b7";
				break;
			case "18daf1db.62924e":
				$RES = "62924ec4";
				break;
			case "18daf1db.62924f":
				$RES = "62924f8002e320";
				break;
			case "18daf1db.629250":
				$RES = "62925080049400";
				break;
			case "18daf1db.629251":
				$RES = "6292518002e320";
				break;
			case "18daf1db.629252":
				$RES = "62925280049400";
				break;
			case "18daf1db.629254":
				$RES = "6292540e92";
				break;
			case "18daf1db.629255":
				$RES = "6292550e98";
				break;
			case "18daf1db.629256":
				$RES = "6292560000000000000000000000000000000000000000000000000000000000000000";
				break;
			case "18daf1db.629257":
				$RES = "6292577f72";
				break;
			case "18daf1db.629258":
				$RES = "6292587f72";
				break;
			case "18daf1db.629259":
				$RES = "62925904";
				break;
			case "18daf1db.62925a":
				$RES = "62925a00";
				break;
			case "18daf1db.62925b":
				$RES = "62925b0000";
				break;
			case "18daf1db.62925c":
				$RES = "62925c01";
				break;
			case "18daf1db.62925d":
				$RES = "62925d7f72";
				break;
			case "18daf1db.62925e":
				$RES = "62925e51594f";
				break;
			case "18daf1db.62925f":
				$RES = "62925f80000000";
				break;
			case "18daf1db.629260":
				$RES = "629260fffffffe";
				break;
			case "18daf1db.629261":
				$RES = "629261093db1";
				break;
			case "18daf1db.629262":
				$RES = "62926280000000";
				break;
			case "18daf1db.629263":
				$RES = "62926380000000";
				break;
			case "18daf1db.629264":
				$RES = "629264000000";
				break;
			case "18daf1db.629265":
				$RES = "62926500000000";
				break;
			case "18daf1db.629266":
				$RES = "62926613";
				break;
			case "18daf1db.629267":
				$RES = "6292672552";
				break;
			case "18daf1db.629268":
				$RES = "6292680064";
				break;
			case "18daf1db.629269":
				$RES = "6292690064";
				break;
			case "18daf1db.62926a":
				$RES = "62926a1ea1248320291d03257023b7235d1c8b23da22c41b642541248e228c1fe02536269a23001e6a2124";
				break;
			case "18daf1db.62926b":
				$RES = "62926b000000";
				break;
			case "18daf1db.62926c":
				$RES = "62926c8000000080000000800000008000000080000000800000008000000080000000800000008000000080000000800000008000000080000000800000008000000080000000800000008000000080000000800000008000000080000000800000008000000080000000800000008000000080000000800000008000000080000000800000008000000080000000800000008000000080000000800000008000000080000000800000008000000080000000800000008000000080000000800000008000000080000000800000008000000080000000830007d0802a001580785e9c8000000082000000800000008000000080000000800000008000000080092ab580000000800000008000000080000000800000008051d1cb80000000800000008000000080007ef480000000800000008000000a8000000080000000800000288000000080000000800000288200000080000000800000008e74000080000000800000008f080000820202028202030282020302820203028202030280030304830700038202000188030004840802028303030383030303830303038303030383030303a24d13608e7414b4a68d1bc68ed80aa0a68519429f4a0e74a685218e98761433a68d1f7295e00f3ca68d17d4940818b28ac821b194160ef98e951d9ca36712e88e5226859d3518768d8a23709ec815988178e1fb81acbff08162bd7781bf2eb68264321f8ab4796081a8d4cf819d7a2e81af1a1888fb8b508151071581a763ee81f5744e8905f24081775cce81e7fa2e81a9d50c8972e578821668c6832f9b24a7130e1aa72919108e2a21119115260e8f1a1d0f800929d9800913ff800902d08008e71c8008e6a58008d95b8008d6398008d2198008c5658008ac718008a9de800892098008821280087fa880087c38800876cf800807838007ffc68007fccf8007f856b24c5142b22f434fd12e394ecd483352d0423f51";
				break;
			case "18daf1db.62926d":
				$RES = "62926d00";
				break;
			case "18daf1db.62926e":
				$RES = "62926e000608000000000908000200000a060000000907000000000000000303000000";
				break;
			case "18daf1db.62926f":
				$RES = "62926f0001020000000002010000000002010000000201000000000004000000000000";
				break;
			case "18daf1db.629270":
				$RES = "6292700606060606060606060606060606060606060606060606060404040606060606";
				break;
			case "18daf1db.629271":
				$RES = "6292712d2d313232333332313131313130323233323132323333332c2c2c2c2c2c2c2c";
				break;
			case "18daf1db.629272":
				$RES = "6292722d2d313232333332313131313130323233323232323333332c2c2c2c2c2c2c2c";
				break;
			case "18daf1db.629273":
				$RES = "629273262c3232323333393e3e404040474b4b4b4b515555555555211d1d1f23232425";
				break;
			case "18daf1db.629274":
				$RES = "62927425272d3232323334393e3e404040474b4b4b4b515555555521211d1d21232425";
				break;
			case "18daf1db.629275":
				$RES = "6292750929d90929920927ed0927d70927c50924870924690923d609224a092236091ebf091eb2091e8b091e3c091cae091c920918f00918d309189c09170f0916f4091412091408091401092acb092ab8092ab7092a91092a83092a4a092a34092a13";
				break;
			case "18daf1db.629276":
				$RES = "6292760929dc0929a50927ff0927d70927c609248a09246b0923fb09225d092237091ecb091eb5091e8d091e62091cc1091c980918f30918d60918c10917250916f5091413091412091408092acd092acb092ab7092a9c092a8c092a4c092a38092a15";
				break;
			case "18daf1db.629277":
				$RES = "629277909ba660909ba4e09095d1809090016090900160908a3120907e9100907e8e009072f4009072f400906d27e0906192209061922090618f2090618da0905bc7609056012090503b0090503820904a73809044b200903ef0a0903ef0a0903ef0a090b2f70090b2f70090b2f70090b2f64090ad212090ad210090ad20e090a74d40";
				break;
			case "18daf1db.629278":
				$RES = "62927890a179e0909ba6609095d3209090016090900160908a314090846100907e91009078c1009072f400906d288090675d00906192209061922090618f20905bc7609056014090503b2090503b00904a75209044b200903ef0a0903ef0a0903ef0a090b2f70090b2f70090b2f70090b2f70090ad21e090ad212090ad210090a74d60";
				break;
			case "18daf1db.629279":
				$RES = "62927908";
				break;
			case "18daf1db.62927a":
				$RES = "62927a00000000000000000000000000000000000000000000000000000000000000009078c1009072f400906d288090675d00906192209061922090618f20905bc7609056014090503b2090503b00904a75209044b200903ef0a0903ef0a0903ef0a090b2f70090b2f70090b2f70090b2f70090ad21e090ad212090ad210090a74d60800000008000000080000000800000008000000080000000800000008000000080000000800000008000000080000000800000008000000080000000800000008000000080000000800000008000000080000000830007d0802a001580785e9c800000008200000080000000800000008000000080000000800000";
				break;
			case "18daf1db.62927b":
				$RES = "62927b0000";
				break;
			case "18daf1db.62927c":
				$RES = "62927c00";
				break;
			case "18daf1db.62927d":
				$RES = "62927d00";
				break;
			case "18daf1db.62927e":
				$RES = "62927e00";
				break;
			case "18daf1db.62927f":
				$RES = "62927f00";
				break;
			case "18daf1db.629280":
				$RES = "62928000000006";
				break;
			case "18daf1db.629281":
				$RES = "62928100";
				break;
			case "18daf1db.629282":
				$RES = "62928232393341303139353852543139333430383034365041413030394152303030";
				break;
			case "18daf1db.6292a0":
				$RES = "7f2231";
				break;
			case "18daf1db.6292c0":
				$RES = "7f2231";
				break;
			case "18daf1db.6292e0":
				$RES = "7f2231";
				break;
			case "18daf1db.629300":
				$RES = "7f2231";
				break;
			case "18daf1db.629320":
				$RES = "7f2231";
				break;
			case "18daf1db.629340":
				$RES = "7f2231";
				break;
			case "18daf1db.629360":
				$RES = "7f2231";
				break;
			case "18daf1db.629380":
				$RES = "7f2231";
				break;
			case "18daf1db.6293a0":
				$RES = "7f2231";
				break;
			case "18daf1db.6293c0":
				$RES = "7f2231";
				break;
			case "18daf1db.6293e0":
				$RES = "7f2231";
				break;
			case "18daf1db.629400":
				$RES = "7f2231";
				break;
			case "18daf1db.629420":
				$RES = "7f2231";
				break;
			case "18daf1db.629440":
				$RES = "7f2231";
				break;
			case "18daf1db.629460":
				$RES = "7f2231";
				break;
			case "18daf1db.629480":
				$RES = "7f2231";
				break;
			case "18daf1db.6294a0":
				$RES = "7f2231";
				break;
			case "18daf1db.6294c0":
				$RES = "7f2231";
				break;
			case "18daf1db.6294e0":
				$RES = "7f2231";
				break;
			case "18daf1db.629500":
				$RES = "7f2231";
				break;
			case "18daf1db.629520":
				$RES = "7f2231";
				break;
			case "18daf1db.629540":
				$RES = "7f2231";
				break;
			case "18daf1db.629560":
				$RES = "7f2231";
				break;
			case "18daf1db.629580":
				$RES = "7f2231";
				break;
			case "18daf1db.6295a0":
				$RES = "7f2231";
				break;
			case "18daf1db.6295c0":
				$RES = "7f2231";
				break;
			case "18daf1db.6295e0":
				$RES = "7f2231";
				break;
			case "18daf1db.629600":
				$RES = "7f2231";
				break;
			case "18daf1db.629620":
				$RES = "7f2231";
				break;
			case "18daf1db.629640":
				$RES = "7f2231";
				break;
			case "18daf1db.629660":
				$RES = "7f2231";
				break;
			case "18daf1db.629680":
				$RES = "7f2231";
				break;
			case "18daf1db.6296a0":
				$RES = "7f2231";
				break;
			case "18daf1db.6296c0":
				$RES = "7f2231";
				break;
			case "18daf1db.6296e0":
				$RES = "7f2231";
				break;
			case "18daf1db.629700":
				$RES = "7f2231";
				break;
			case "18daf1db.629720":
				$RES = "7f2231";
				break;
			case "18daf1db.629740":
				$RES = "7f2231";
				break;
			case "18daf1db.629760":
				$RES = "7f2231";
				break;
			case "18daf1db.629780":
				$RES = "7f2231";
				break;
			case "18daf1db.6297a0":
				$RES = "7f2231";
				break;
			case "18daf1db.6297c0":
				$RES = "7f2231";
				break;
			case "18daf1db.6297e0":
				$RES = "7f2231";
				break;
			case "18daf1db.629800":
				$RES = "7f2231";
				break;
			case "18daf1db.629820":
				$RES = "7f2231";
				break;
			case "18daf1db.629840":
				$RES = "7f2231";
				break;
			case "18daf1db.629860":
				$RES = "7f2231";
				break;
			case "18daf1db.629880":
				$RES = "7f2231";
				break;
			case "18daf1db.6298a0":
				$RES = "7f2231";
				break;
			case "18daf1db.6298c0":
				$RES = "7f2231";
				break;
			case "18daf1db.6298e0":
				$RES = "7f2231";
				break;
			case "18daf1db.629900":
				$RES = "7f2231";
				break;
			case "18daf1db.629920":
				$RES = "7f2231";
				break;
			case "18daf1db.629940":
				$RES = "7f2231";
				break;
			case "18daf1db.629960":
				$RES = "7f2231";
				break;
			case "18daf1db.629980":
				$RES = "7f2231";
				break;
			case "18daf1db.6299a0":
				$RES = "7f2231";
				break;
			case "18daf1db.6299c0":
				$RES = "7f2231";
				break;
			case "18daf1db.6299e0":
				$RES = "7f2231";
				break;
			case "18daf1db.629a00":
				$RES = "7f2231";
				break;
			case "18daf1db.629a20":
				$RES = "7f2231";
				break;
			case "18daf1db.629a40":
				$RES = "7f2231";
				break;
			case "18daf1db.629a60":
				$RES = "7f2231";
				break;
			case "18daf1db.629a80":
				$RES = "7f2231";
				break;
			case "18daf1db.629aa0":
				$RES = "7f2231";
				break;
			case "18daf1db.629ac0":
				$RES = "7f2231";
				break;
			case "18daf1db.629ae0":
				$RES = "7f2231";
				break;
			case "18daf1db.629b00":
				$RES = "7f2231";
				break;
			case "18daf1db.629b20":
				$RES = "7f2231";
				break;
			case "18daf1db.629b40":
				$RES = "7f2231";
				break;
			case "18daf1db.629b60":
				$RES = "7f2231";
				break;
			case "18daf1db.629b80":
				$RES = "7f2231";
				break;
			case "18daf1db.629ba0":
				$RES = "7f2231";
				break;
			case "18daf1db.629bc0":
				$RES = "7f2231";
				break;
			case "18daf1db.629be0":
				$RES = "7f2231";
				break;
			case "18daf1db.629c00":
				$RES = "7f2231";
				break;
			case "18daf1db.629c20":
				$RES = "7f2231";
				break;
			case "18daf1db.629c40":
				$RES = "7f2231";
				break;
			case "18daf1db.629c60":
				$RES = "7f2231";
				break;
			case "18daf1db.629c80":
				$RES = "7f2231";
				break;
			case "18daf1db.629ca0":
				$RES = "7f2231";
				break;
			case "18daf1db.629cc0":
				$RES = "7f2231";
				break;
			case "18daf1db.629ce0":
				$RES = "7f2231";
				break;
			case "18daf1db.629d00":
				$RES = "7f2231";
				break;
			case "18daf1db.629d20":
				$RES = "7f2231";
				break;
			case "18daf1db.629d40":
				$RES = "7f2231";
				break;
			case "18daf1db.629d60":
				$RES = "7f2231";
				break;
			case "18daf1db.629d80":
				$RES = "7f2231";
				break;
			case "18daf1db.629da0":
				$RES = "7f2231";
				break;
			case "18daf1db.629dc0":
				$RES = "7f2231";
				break;
			case "18daf1db.629de0":
				$RES = "7f2231";
				break;
			case "18daf1db.629e00":
				$RES = "7f2231";
				break;
			case "18daf1db.629e20":
				$RES = "7f2231";
				break;
			case "18daf1db.629e40":
				$RES = "7f2231";
				break;
			case "18daf1db.629e60":
				$RES = "7f2231";
				break;
			case "18daf1db.629e80":
				$RES = "7f2231";
				break;
			case "18daf1db.629ea0":
				$RES = "7f2231";
				break;
			case "18daf1db.629ec0":
				$RES = "7f2231";
				break;
			case "18daf1db.629ee0":
				$RES = "7f2231";
				break;
			case "18daf1db.629f00":
				$RES = "7f2231";
				break;
			case "18daf1db.629f20":
				$RES = "7f2231";
				break;
			case "18daf1db.629f40":
				$RES = "7f2231";
				break;
			case "18daf1db.629f60":
				$RES = "7f2231";
				break;
			case "18daf1db.629f80":
				$RES = "7f2231";
				break;
			case "18daf1db.629fa0":
				$RES = "7f2231";
				break;
			case "18daf1db.629fc0":
				$RES = "7f2231";
				break;
			case "18daf1db.629fe0":
				$RES = "7f2231";
				break;
			case "18daf1db.62f000":
				$RES = "62f00000040000";
				break;
			case "18daf1db.62f010":
				$RES = "7f2231";
				break;
			case "18daf1db.62f012":
				$RES = "62f01232393341303939303252";
				break;
			case "18daf1db.62f020":
				$RES = "7f2231";
				break;
			case "18daf1db.62f0a0":
				$RES = "62f0a03201";
				break;
			case "18daf1db.62f0a5":
				$RES = "7f2231";
				break;
			case "18daf1db.62f100":
				$RES = "7f2231";
				break;
			case "18daf1db.62f111":
				$RES = "7f2231";
				break;
			case "18daf1db.62f120":
				$RES = "7f2231";
				break;
			case "18daf1db.62f121":
				$RES = "7f2231";
				break;
			case "18daf1db.62f180":
				$RES = "7f2231";
				break;
			case "18daf1db.62f182":
				$RES = "62f1820180";
				break;
			case "18daf1db.62f187":
				$RES = "62f18732393341303939303252";
				break;
			case "18daf1db.62f188":
				$RES = "62f18830313233343536373846";
				break;
			case "18daf1db.62f18c":
				$RES = "62f18c3139333430383034365041413030394152303030";
				break;
			case "18daf1db.62f190":
				$RES = "7f2231";
				break;
			case "18daf1db.62f191":
				$RES = "62f19132393341303139353852";
				break;
			case "18daf1db.62f196":
				$RES = "62f19630303030303030303030";
				break;
			case "18daf1db.62f197":
				$RES = "62f1973030303030303030303030303030303030303030303030303030303030303030";
				break;
			case "18daf1db.62f1a1":
				$RES = "62f1a100000000000000000000";
				break;
			case "18daf1db.62f400":
				$RES = "62f40098180011";
				break;
			case "18daf1db.62f401":
				$RES = "62f40100000400";
				break;
			case "18daf1db.62f404":
				$RES = "62f40400";
				break;
			case "18daf1db.62f405":
				$RES = "62f40500";
				break;
			case "18daf1db.62f40c":
				$RES = "62f40c0000";
				break;
			case "18daf1db.62f40d":
				$RES = "62f40d00";
				break;
			case "18daf1db.62f41c":
				$RES = "62f41c00";
				break;
			case "18daf1db.62f420":
				$RES = "62f42000002001";
				break;
			case "18daf1db.62f421":
				$RES = "7f2231";
				break;
			case "18daf1db.62f430":
				$RES = "7f2231";
				break;
			case "18daf1db.62f431":
				$RES = "7f2231";
				break;
			case "18daf1db.62f433":
				$RES = "62f43300";
				break;
			case "18daf1db.62f440":
				$RES = "62f44044800021";
				break;
			case "18daf1db.62f441":
				$RES = "7f2231";
				break;
			case "18daf1db.62f442":
				$RES = "62f4423278";
				break;
			case "18daf1db.62f449":
				$RES = "62f44900";
				break;
			case "18daf1db.62f45b":
				$RES = "62f45b54";
				break;
			case "18daf1db.62f480":
				$RES = "62f48000000040";
				break;
			case "18daf1db.62f49a":
				$RES = "62f49a3201f4400000";
				break;
			case "18daf1db.62f804":
				$RES = "7f2231";
				break;
			case "18daf1db.62f806":
				$RES = "7f2231";
				break;
			case "18daf1db.62f80a":
				$RES = "7f2231";
				break;
			case "18daf1db.62f80f":
				$RES = "7f2231";
				break;
			case "18daf1db.7e00":
				$RES = "7e00";
				break;

			default:
				$RES = "-E-Frame not found ($SID)";
				break;
		}
		break;

	case "g":
		break;

	case "n":
	default:
		break;
}

echo $ID . "," . $RES;

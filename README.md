This code emulates a CanSee dongle hooked to a Renault ZOE. Primary purpose is to test the UI without the need to sit in the car.

- It uses the CanSee protocol, but over http. CanZE supports this mode.
- It requires a php and https enabled webserver to run.
- It serves limited frames, almost none dynamic.
- There are two versions, CanSeeEmulator (for original ZOE) and CanSeeEmulator (for Ph2, also called ZE50).
